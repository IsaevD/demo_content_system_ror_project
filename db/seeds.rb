ModulesD::Engine.load_seed
SystemMainItemsD::Engine.load_seed
AccessRightsD::Engine.load_seed
SystemUserActionsLogD::Engine.load_seed

# Импорт информации о модулях
modules = {
    "project_categories" => {
        :module_name => "admin",
        :name => "Категории проектов",
        :description => "Предназначен для управления категорями проектов",
        :git_url => ""
    },
    "projects" => {
        :module_name => "admin",
        :name => "Проекты",
        :description => "Предназначен для управления проектами",
        :git_url => ""
    },
    "news_items" => {
        :module_name => "admin",
        :name => "Новости",
        :description => "Предназначен для управления новостями",
        :git_url => ""
    },
    "instructions" => {
        :module_name => "admin",
        :name => "Инструкции",
        :description => "Предназначен для управления инструкциями",
        :git_url => ""
    },
    "key_word_states" => {
        :module_name => "admin",
        :name => "Состояния ключевых слов",
        :description => "Предназначен для управления статусами ключевых слов",
        :git_url => ""
    },
    "key_word_rubrics" => {
      :module_name => "admin",
      :name => "Рубрики ключевых слов",
      :description => "Предназначен для управления рубриками ключевых слов",
      :git_url => ""
    },
    "key_word_groups" => {
        :module_name => "admin",
        :name => "Группы ключевых слов",
        :description => "Предназначен для управления группами ключевых слов",
        :git_url => ""
    },
    "key_words" => {
        :module_name => "admin",
        :name => "Ключевые слова",
        :description => "Предназначен для управления ключевыми словами",
        :git_url => ""
    },
    "task_stages" => {
        :module_name => "admin",
        :name => "Этапы задач",
        :description => "Предназначен для управления этапами задач",
        :git_url => ""
    },
    "task_states" => {
        :module_name => "admin",
        :name => "Состояния задач",
        :description => "Предназначен для управления состояниями задач",
        :git_url => ""
    },
    "tasks" => {
        :module_name => "admin",
        :name => "Задачи",
        :description => "Предназначен для управления задачами",
        :git_url => ""
    },
    "subtasks" => {
        :module_name => "admin",
        :name => "Подзадачи",
        :description => "Предназначен для управления подзадачами",
        :git_url => ""
    },
    "articles" => {
        :module_name => "admin",
        :name => "Статьи",
        :description => "Предназначен для управления статьями",
        :git_url => ""
    },
    "purchase_links" => {
        :module_name => "admin",
        :name => "Покупные сслыки",
        :description => "Предназначен для управления покупными ссылками",
        :git_url => ""
    },
    "index" => {
        :module_name => "admin",
        :name => "Главная страница",
        :description => "Предназначен для управления личной страницой ролей",
        :git_url => ""
    }
}
modules.each do |key, value|
  if (ModulesD::ModuleItem.find_by(:alias => key).nil?)
    ModulesD::ModuleItem.new({ :alias => key, :module_name => value[:module_name], :name => value[:name], :description => value[:description], :git_url => value[:git_url] }).save
  end
end
puts "Custom modules import success"

puts "*********************************"
# Импорт ролей пользователей
puts "--- System User Roles start import"
user_roles = {
    "administrator" => {
        :name => "Администратор",
        :description => "Полные права в системе",
        :rate => 100
    },
    "key_manager" => {
        :name => "Key-manager",
        :description => "Работает с ключевыми словами в системе",
        :rate => 100
    },
    "link_manager" => {
        :name => "Link-manager",
        :description => "Работает с закупкой ссылок",
        :rate => 100
    },
    "content_manager" => {
        :name => "Content-manager",
        :description => "Работает с публикацией контента",
        :rate => 100
    }
}
user_roles.each do |key, value|
  if (SystemMainItemsD::SystemUserRole.find_by(:alias => key).nil?)
    SystemMainItemsD::SystemUserRole.new({ :alias => key, :name => value[:name], :description => value[:description], :rate => value[:rate], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
  end
end
puts "+++ System User Roles import success"
puts "*********************************"
puts "--- System Users start import"
system_users = {
    "key_manager" => {
        :first_name => "Иван",
        :middle_name => "Иванович",
        :last_name => "Иванов",
        :email => "key_manager@test.com",
        :rate => 100,
        :system_user_role_alias => "key_manager"
    },
    "key_manager_2" => {
        :first_name => "Иван",
        :middle_name => "Иванович",
        :last_name => "Иванов",
        :email => "key_manager_2@test.com",
        :rate => 100,
        :system_user_role_alias => "key_manager"
    },
    "link_manager" => {
        :first_name => "Петр",
        :middle_name => "Петрович",
        :last_name => "Петров",
        :email => "link_manager@test.com",
        :rate => 100,
        :system_user_role_alias => "link_manager"
    },
    "link_manager_2" => {
        :first_name => "Петр",
        :middle_name => "Петрович",
        :last_name => "Петров",
        :email => "link_manager_2@test.com",
        :rate => 100,
        :system_user_role_alias => "link_manager"
    },
    "content_manager" => {
        :first_name => "Сергей",
        :middle_name => "Сергеевич",
        :last_name => "Сергеев",
        :email => "content_manager@test.com",
        :rate => 100,
        :system_user_role_alias => "content_manager"
    },
    "content_manager_2" => {
        :first_name => "Сергей",
        :middle_name => "Сергеевич",
        :last_name => "Сергеев",
        :email => "content_manager_2@test.com",
        :rate => 100,
        :system_user_role_alias => "content_manager"
    },
    "administrator" => {
        :first_name => "Василий",
        :middle_name => "Васильевич",
        :last_name => "Василтев",
        :email => "administrator@test.com",
        :rate => 100,
        :system_user_role_alias => "administrator"
    },
    "root" => {
        :first_name => "Иван",
        :middle_name => "Иванович",
        :last_name => "Иванов",
        :email => "root@test.com",
        :rate => 100,
        :system_user_role_alias => "root"
    }
}
system_users.each do |key, value|
    if (SystemMainItemsD::SystemUser.find_by(:login => key).nil?)
      system_user = SystemMainItemsD::SystemUser.new({
         :login => key,
         :first_name => value[:first_name],
         :middle_name => value[:middle_name],
         :last_name => value[:last_name],
         :email => value[:email],
         :rate => value[:rate],
         :system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => value[:system_user_role_alias]).id,
         :system_state_id => SystemMainItemsD::SystemState.active_state.id
      })
      system_user.password = "123456"
      system_user.password_confirmation = "123456"
      system_user.save
    end
end
puts "+++ System Users import success"
puts "*********************************"
puts "--- Access Rights start import"
access_rights = {
    SystemMainItemsD::SystemUserRole.find_by(:alias => "link_manager").id => [
        ModulesD::ModuleItem.find_by(:alias => "index").id,
        ModulesD::ModuleItem.find_by(:alias => "subtasks").id,
        ModulesD::ModuleItem.find_by(:alias => "articles").id,
        ModulesD::ModuleItem.find_by(:alias => "purchase_links").id
    ],
    SystemMainItemsD::SystemUserRole.find_by(:alias => "content_manager").id => [
        ModulesD::ModuleItem.find_by(:alias => "index").id,
        ModulesD::ModuleItem.find_by(:alias => "subtasks").id,
    ],
    SystemMainItemsD::SystemUserRole.find_by(:alias => "key_manager").id => [
        ModulesD::ModuleItem.find_by(:alias => "index").id,
        ModulesD::ModuleItem.find_by(:alias => "key_word_groups").id,
        ModulesD::ModuleItem.find_by(:alias => "key_words").id,
        ModulesD::ModuleItem.find_by(:alias => "tasks").id
    ],
    SystemMainItemsD::SystemUserRole.find_by(:alias => "administrator").id => [
        ModulesD::ModuleItem.find_by(:alias => "project_categories").id,
        ModulesD::ModuleItem.find_by(:alias => "projects").id,
        ModulesD::ModuleItem.find_by(:alias => "news_items").id,
        ModulesD::ModuleItem.find_by(:alias => "instructions").id,
        ModulesD::ModuleItem.find_by(:alias => "key_word_states").id,
        ModulesD::ModuleItem.find_by(:alias => "key_word_rubrics").id,
        ModulesD::ModuleItem.find_by(:alias => "key_word_groups").id,
        ModulesD::ModuleItem.find_by(:alias => "key_words").id,
        ModulesD::ModuleItem.find_by(:alias => "task_stages").id,
        ModulesD::ModuleItem.find_by(:alias => "task_states").id,
        ModulesD::ModuleItem.find_by(:alias => "tasks").id,
        ModulesD::ModuleItem.find_by(:alias => "subtasks").id,
        ModulesD::ModuleItem.find_by(:alias => "articles").id,
        ModulesD::ModuleItem.find_by(:alias => "purchase_links").id,
        ModulesD::ModuleItem.find_by(:alias => "index").id
    ],
    SystemMainItemsD::SystemUserRole.find_by(:alias => "root").id => [
        ModulesD::ModuleItem.find_by(:alias => "project_categories").id,
        ModulesD::ModuleItem.find_by(:alias => "projects").id,
        ModulesD::ModuleItem.find_by(:alias => "news_items").id,
        ModulesD::ModuleItem.find_by(:alias => "instructions").id,
        ModulesD::ModuleItem.find_by(:alias => "key_word_states").id,
        ModulesD::ModuleItem.find_by(:alias => "key_word_rubrics").id,
        ModulesD::ModuleItem.find_by(:alias => "key_word_groups").id,
        ModulesD::ModuleItem.find_by(:alias => "key_words").id,
        ModulesD::ModuleItem.find_by(:alias => "task_stages").id,
        ModulesD::ModuleItem.find_by(:alias => "task_states").id,
        ModulesD::ModuleItem.find_by(:alias => "tasks").id,
        ModulesD::ModuleItem.find_by(:alias => "subtasks").id,
        ModulesD::ModuleItem.find_by(:alias => "articles").id,
        ModulesD::ModuleItem.find_by(:alias => "purchase_links").id,
        ModulesD::ModuleItem.find_by(:alias => "index").id,
        ModulesD::ModuleItem.find_by(:alias => "system_users").id,
        ModulesD::ModuleItem.find_by(:alias => "system_user_roles").id,
        ModulesD::ModuleItem.find_by(:alias => "access_rights").id,
        ModulesD::ModuleItem.find_by(:alias => "actions_log_records").id
    ]
}
access_rights.each do |key, value|
  if (AccessRightsD::AccessRight.find_by(:system_user_role_id => key).nil?)
    AccessRightsD::AccessRight.new({:system_user_role_id => key }).save
  end
  access_right_id = AccessRightsD::AccessRight.find_by(:system_user_role_id => key).id
  value.each do |item|
    if AccessRightsD::ModuleRight.find_by(:access_right_id => access_right_id, :module_item_id => item).nil?
      AccessRightsD::ModuleRight.new({ :access_right_id  => access_right_id, :module_item_id => item }).save
    end
  end
end
puts "+++ Access Rights import success"
puts "*****"



puts "--- Project Categories start import"
project_categories = {
    "test_project_category" => {
        :name => "Тестовая категория",
        :description => "Категория для демонстрации функционала"
    }
}
project_categories.each do |key, value|
    if (ProjectCategory.find_by(:alias => key).nil?)
        ProjectCategory.new({ :alias => key, :name => value[:name], :description => value[:description], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
    end
end
puts "+++ Project Categories import success"


puts "*********************************"


puts "--- Keywords states start import"
keywords_states = {
    "usage" => {
        :name => "Употребление"
    }
}
keywords_states.each do |key, value|
    if (KeyWordState.find_by(:alias => key).nil?)
        KeyWordState.new({ :alias => key, :name => value[:name], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
    end
end
puts "+++ Keywords states import success"


puts "*********************************"


puts "--- Keywords rubrics start import"
keywords_rubrics = {
    "test_rubric" => {
        :name => "Тестовая рубрика"
    }
}
keywords_rubrics.each do |key, value|
    if (KeyWordRubric.find_by(:alias => key).nil?)
        KeyWordRubric.new({ :alias => key, :name => value[:name], :system_state_id => SystemMainItemsD::SystemState.active_state.id }).save
    end
end
puts "+++ Keywords rubrics import success"


puts "*********************************"


puts "--- Task Stages start import"
task_stages = {
    "initialize" => {
        :name => "Инициализация"
    },
    "key_manager_work_stage" => {
        :name => "Работа key-менеджера"
    },
    "content_and_link_manager_work_stage" => {
        :name => "Работа content и link-менеджера"
    },
    "completed" => {
        :name => "Завершена"
    }
}
task_stages.each do |key, value|
    if (TaskStage.find_by(:alias => key).nil?)
        TaskStage.new({ :alias => key, :name => value[:name] }).save
    end
end
puts "+++ Task Stages import success"


puts "*********************************"


puts "--- Subtask Stages start import"
subtask_stages = {
    "initialize_stage" => {
        :name => "Инициализация"
    },
    "auction_content_manager_work_stage" => {
        :name => "Аукцион для content-менеджер"
    },
    "content_manager_work_stage" => {
        :name => "Работа content-менеджера"
    },
    "link_manager_work_stage" => {
        :name => "Работа link-менеджера"
    },
    "completed" => {
        :name => "Завершена"
    }
}
subtask_stages.each do |key, value|
  if (SubtaskStage.find_by(:alias => key).nil?)
    SubtaskStage.new({ :alias => key, :name => value[:name] }).save
  end
end
puts "+++ Subtask Stages import success"

puts "*********************************"

puts "--- Task States start import"
task_states = {
    "in_progress" => {
        :name => "В работе"
    },
    "for_revision" => {
        :name => "На доработку"
    },
    "complete" => {
        :name => "Выполнена"
    }
}
task_states.each do |key, value|
    if (TaskState.find_by(:alias => key).nil?)
        TaskState.new({ :alias => key, :name => value[:name] }).save
    end
end
puts "+++ Task States import success"


puts "*********************************"



