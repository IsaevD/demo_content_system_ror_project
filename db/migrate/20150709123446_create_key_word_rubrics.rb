class CreateKeyWordRubrics < ActiveRecord::Migration
  def change
    create_table :key_word_rubrics do |t|
      t.string :alias
      t.string :name
      t.integer :system_state_id
      t.timestamps
    end
  end
end
