class CreateSubtasks < ActiveRecord::Migration
  def change
    create_table :subtasks do |t|
      t.string :name
      t.text :description
      t.integer :key_word_group_id
      t.integer :task_id
      t.string :title
      t.text :content
      t.string :meta_title
      t.string :meta_description
      t.integer :content_manager_id
      t.timestamps
    end
  end
end
