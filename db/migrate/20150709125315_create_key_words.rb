class CreateKeyWords < ActiveRecord::Migration
  def change
    create_table :key_words do |t|
      t.string :name
      t.integer :entry_count
      t.integer :key_word_state_id
      t.integer :system_state_id
      t.timestamps
    end
  end
end
