class AddDateToSubTask < ActiveRecord::Migration
  def change
    add_column :subtasks, :date, :datetime
  end
end
