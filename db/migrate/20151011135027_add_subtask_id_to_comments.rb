class AddSubtaskIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :subtask_id, :integer
  end
end
