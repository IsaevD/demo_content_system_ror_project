class ChangeStageInSubtask < ActiveRecord::Migration

  def up
    add_column :subtasks, :subtask_stage_id, :integer
    remove_column :subtasks, :task_stage_id, :integer
  end

  def down
    remove_column :subtasks, :subtask_stage_id, :integer
    add_column :subtasks, :task_state_id, :integer
  end

end
