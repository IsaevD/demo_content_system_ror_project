class CreateKeyWordGroups < ActiveRecord::Migration
  def change
    create_table :key_word_groups do |t|
      t.string :name
      t.text :description
      t.integer :volume_of
      t.integer :volume_to
      t.string :exact_entry
      t.integer :key_word_rubric_id
      t.integer :system_state_id
      t.timestamps
    end
  end
end
