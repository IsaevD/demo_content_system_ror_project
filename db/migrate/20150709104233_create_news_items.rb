class CreateNewsItems < ActiveRecord::Migration
  def change
    create_table :news_items do |t|
      t.string :name
      t.text :description
      t.datetime :date
      t.integer :system_state_id
      t.timestamps
    end
  end
end
