class AddTaskIdToKeywords < ActiveRecord::Migration
  def change
    add_column :key_word_groups, :task_id, :integer
  end
end
