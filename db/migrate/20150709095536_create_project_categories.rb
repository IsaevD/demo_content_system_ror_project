class CreateProjectCategories < ActiveRecord::Migration
  def change
    create_table :project_categories do |t|
      t.string :alias
      t.string :name
      t.text :description
      t.integer :system_state_id
      t.timestamps
    end
  end
end
