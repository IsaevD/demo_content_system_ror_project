class AddUserToKeyWordsAndKeyGroups < ActiveRecord::Migration
  def change
    add_column :key_words, :key_manager_id, :integer
    add_column :key_word_groups, :key_manager_id, :integer
  end
end
