class CreateTaskKeyWordGroups < ActiveRecord::Migration
  def change
    create_table :task_key_word_groups do |t|
      t.integer :task_id
      t.integer :key_word_group_id
      t.timestamps
    end
  end
end
