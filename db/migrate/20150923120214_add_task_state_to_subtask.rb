class AddTaskStateToSubtask < ActiveRecord::Migration
  def change
    add_column :subtasks, :task_state_id, :integer
  end
end
