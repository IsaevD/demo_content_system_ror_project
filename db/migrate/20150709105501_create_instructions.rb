class CreateInstructions < ActiveRecord::Migration
  def change
    create_table :instructions do |t|
      t.string :name
      t.text :content
      t.integer :system_user_role_id
      t.integer :system_state_id
      t.timestamps
    end
  end
end
