class DestroyOldTasks < ActiveRecord::Migration
  def change
    drop_table :task_key_word_groups
    drop_table :task_comments
  end
end
