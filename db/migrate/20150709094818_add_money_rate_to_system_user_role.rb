class AddMoneyRateToSystemUserRole < ActiveRecord::Migration
  def change
    add_column :system_main_items_d_system_user_roles, :rate, :decimal
  end
end
