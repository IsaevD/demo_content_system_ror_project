class AddNewAttributesToPurchaseLinks < ActiveRecord::Migration
  def change
    add_column :purchase_links, :link_manager_id, :integer
  end
end
