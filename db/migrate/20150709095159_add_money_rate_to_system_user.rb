class AddMoneyRateToSystemUser < ActiveRecord::Migration
  def change
    add_column :system_main_items_d_system_users, :rate, :decimal
  end
end
