class AddNewAttributesToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :subtask_id, :integer
    add_column :articles, :link_manager_id, :integer
  end
end
