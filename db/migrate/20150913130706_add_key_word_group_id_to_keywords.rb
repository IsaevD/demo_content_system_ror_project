class AddKeyWordGroupIdToKeywords < ActiveRecord::Migration
  def change
    add_column :key_words, :key_word_group_id, :integer
  end
end
