class CreateTaskContentManagers < ActiveRecord::Migration
  def change
    create_table :task_content_managers do |t|
      t.integer :task_id
      t.integer :system_user_id
      t.timestamps
    end
  end
end
