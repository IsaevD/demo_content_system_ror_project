class CreateTaskComments < ActiveRecord::Migration
  def change
    create_table :task_comments do |t|
      t.text :content
      t.integer :system_user_id
      t.timestamps
    end
  end
end
