class CreateTaskStages < ActiveRecord::Migration
  def change
    create_table :task_stages do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
