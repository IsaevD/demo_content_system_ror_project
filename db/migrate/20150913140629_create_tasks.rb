class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.text :description
      t.integer :key_manager_id
      t.integer :link_manager_id
      t.integer :task_stage_id
      t.integer :task_state_id
      t.timestamps
    end
  end
end
