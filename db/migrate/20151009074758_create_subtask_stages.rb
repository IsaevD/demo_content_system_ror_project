class CreateSubtaskStages < ActiveRecord::Migration
  def change
    create_table :subtask_stages do |t|
      t.string :alias
      t.string :name
      t.timestamps
    end
  end
end
