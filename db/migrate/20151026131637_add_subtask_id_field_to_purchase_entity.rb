class AddSubtaskIdFieldToPurchaseEntity < ActiveRecord::Migration
  def change
    add_column :purchase_links, :subtask_id, :integer
  end
end
