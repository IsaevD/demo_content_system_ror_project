class AddTaskStageToSubtask < ActiveRecord::Migration
  def change
    add_column :subtasks, :task_stage_id, :integer
  end
end
