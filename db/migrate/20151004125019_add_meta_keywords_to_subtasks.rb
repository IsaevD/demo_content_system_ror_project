class AddMetaKeywordsToSubtasks < ActiveRecord::Migration
  def change
    add_column :subtasks, :meta_keywords, :text
  end
end
