class CreatePurchaseLinks < ActiveRecord::Migration
  def change
    create_table :purchase_links do |t|
      t.string :name
      t.string :link
      t.integer :price
      t.text :comment
      t.integer :article_id
      t.timestamps
    end
  end
end
