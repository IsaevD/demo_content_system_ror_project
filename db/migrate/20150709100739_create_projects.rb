class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.string :url
      t.text :description
      t.integer :project_category_id
      t.integer :key_manager_id
      t.integer :link_manager_id
      t.integer :system_state_id
      t.timestamps
    end
  end
end
