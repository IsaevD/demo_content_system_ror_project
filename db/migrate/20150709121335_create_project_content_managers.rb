class CreateProjectContentManagers < ActiveRecord::Migration
  def change
    create_table :project_content_managers do |t|
      t.integer :project_id
      t.integer :system_user_id
      t.timestamps
    end
  end
end
