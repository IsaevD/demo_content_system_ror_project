Rails.application.routes.draw do

  scope module: "admin" do
    root "index#index"
    mount ModulesD::Engine => "/"
    mount SystemMainItemsD::Engine => "/"
    mount SystemUsersAuthorizeD::Engine => "/"
    mount AccessRightsD::Engine => "/"
    mount SystemUserActionsLogD::Engine => "/"
    match '/main_page', to: "index#index", via: "get"
    match '/news/:id', to: "index#news", via: "get"
    match '/instruction_view/:id', to: "index#instruction_view", via: "get"
    match '/auction', to: "index#auction", via: "get"

    # Key Manager Templates
    match '/key_manager_tasks', to: "key_manager_templates#tasks", via: "get"

    match '/ajax_get_key_word_group', to: "key_word_groups#ajax_get_key_word_group", via: "post"
    match '/ajax_get_article', to: "articles#ajax_get_article", via: "post"
    match '/ajax_get_purchase_link', to: "purchase_links#ajax_get_purchase_link", via: "post"
    match '/ajax_add_new_comment', to: "tasks#ajax_add_new_comment", via: "post"
    match '/ajax_add_new_comment_to_subtask', to: "subtasks#ajax_add_new_comment_to_subtask", via: "post"
    match '/ajax_get_comments_by_task_id', to: "tasks#ajax_get_comments_by_task_id", via: "post"
    match '/ajax_get_comments_by_subtask_id', to: "subtasks#ajax_get_comments_by_subtask_id", via: "post"
    match '/ajax_get_subtask_from_auction', to: "index#ajax_get_subtask_from_auction", via: "post"
    match '/ajax_auction_subtasks', to: "index#ajax_auction_subtasks", via: "post"

    resources :project_categories
    resources :projects
    resources :news_items
    resources :instructions
    resources :key_word_states
    resources :key_word_rubrics
    resources :key_word_groups
    resources :key_words
    resources :task_stages
    resources :task_states
    resources :tasks
    resources :subtasks
    resources :articles
    resources :purchase_links

  end

end
