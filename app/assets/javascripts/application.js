// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require visual_interfaces_d/application
//= require number_mask
//= require forms/forms
//= require modules/key_word_group
//= require modules/task/key_word_groups
//= require modules/task/comment
//= require modules/auction
//= require_tree .

$(function(){
    openTableFilterBar();
    openMenuItem();
    //initTaskKeyGroups();
    initCardNav();
    //initTaskCommentAdd();
    //removeKeyWordInGroup();
    //addKeyWordToKeyWordGroup();

    // Инициализация действий с ключевыми словами из-под карточки группы ключевых слов
    initializeActionsWithKeyWordInKeyWordGroup();
    // Инициализация действий с группами ключевых слов из-под карточки задачи
    initializeActionWithKeyWordsGroupsInTask();
    // Инициализация действий с покупными ссылками из-под карточки статьи
    initializeActionsWithPurchaseLinksInArticle();
    // Инициализация действий с комментариями в задаче
    addCommentInTask();
    // Инициализация действий с комментариями в подзадаче
    addCommentInSubtask();
    // Инициализация аукциона
    initializeSubtasksAuction();
    // Инициализация действий со статьями из-под карточки задачи
    initializeActionWithArticlesInSubtask();


    //addPurchaseLinkToArticle();
    //getSubtaskFromAuction();
    initializeHelpBtn();
    initializeForms();
    //removePurchaseLink();
    //initSubtaskArticles();
    if($("#project_category_alias").length != 0) {
        initAutoComplete($("#project_category_name"), $("#project_category_alias"));
    }
    if($("#key_word_state_alias").length != 0) {
        initAutoComplete($("#key_word_state_name"), $("#key_word_state_alias"));
    }
    if($("#key_word_rubric_alias").length != 0) {
        initAutoComplete($("#key_word_rubric_name"), $("#key_word_rubric_alias"));
    }
    if($("#task_stage_alias").length != 0) {
        initAutoComplete($("#task_stage_name"), $("#task_stage_alias"));
    }
    if($("#task_state_alias").length != 0) {
        initAutoComplete($("#task_state_name"), $("#task_state_alias"));
    }
    setCurrentDateToObject ( $('#new_news_item #news_item_date') );
});



// Функция интерфейса
function openTableFilterBar() {
    $(".open_bar").unbind("click");
    $(".open_bar").click(function(){
        if ($("#filter_form").is(":visible"))
            $("#filter_form").hide();
        else
            $("#filter_form").show();
    });
}

function openMenuItem() {
    $(".has_second_menu").unbind("click");
    $(".first_menu_item.has_second_menu").parent().click(function(){
        obj = $(this).children(".second_menu_item");
        if(obj.is(":visible")) {
            obj.hide();
            $(this).children(".first_menu_item").children(".icon-arrow-up").removeClass("icon-arrow-up").addClass("icon-arrow-down");
        }
        else {
            $(obj).show();
            $(this).children(".first_menu_item").children(".icon-arrow-down").removeClass("icon-arrow-down").addClass("icon-arrow-up");
        }
    });
}

function initAutoComplete(object, children) {
    $(object).change(function(){
        if($(children).val() == "") {
            if ($(object).val() != "") {
                $(children).val(transliterate($(object).val()));
            }
        }
    });
}

function transliterate(word){
    var answer = ""
        , a = {};

    a["Ё"]="YO";a["Й"]="I";a["Ц"]="TS";a["У"]="U";a["К"]="K";a["Е"]="E";a["Н"]="N";a["Г"]="G";a["Ш"]="SH";a["Щ"]="SCH";a["З"]="Z";a["Х"]="H";a["Ъ"]="'";
    a["ё"]="yo";a["й"]="i";a["ц"]="ts";a["у"]="u";a["к"]="k";a["е"]="e";a["н"]="n";a["г"]="g";a["ш"]="sh";a["щ"]="sch";a["з"]="z";a["х"]="h";a["ъ"]="'";
    a["Ф"]="F";a["Ы"]="I";a["В"]="V";a["А"]="a";a["П"]="P";a["Р"]="R";a["О"]="O";a["Л"]="L";a["Д"]="D";a["Ж"]="ZH";a["Э"]="E";
    a["ф"]="f";a["ы"]="i";a["в"]="v";a["а"]="a";a["п"]="p";a["р"]="r";a["о"]="o";a["л"]="l";a["д"]="d";a["ж"]="zh";a["э"]="e";
    a["Я"]="Ya";a["Ч"]="CH";a["С"]="S";a["М"]="M";a["И"]="I";a["Т"]="T";a["Ь"]="'";a["Б"]="B";a["Ю"]="YU";
    a["я"]="ya";a["ч"]="ch";a["с"]="s";a["м"]="m";a["и"]="i";a["т"]="t";a["ь"]="'";a["б"]="b";a["ю"]="yu";

    for (i in word){
        if (word.hasOwnProperty(i)) {
            if (a[word[i]] === undefined){
                answer += word[i];
            } else {
                answer += a[word[i]];
            }
        }
    }
    return answer;
}

/*
function initTaskKeyGroups() {
    $("#add_key_words").click(function(){
        if ($("#task_key_groups_ids .key_word_group").length == 0) {
            value = $("#choose_key_word_groups").val();
            $.ajax({
                type: "POST",
                data: {"group_id": value },
                url: "/ajax_get_key_word_group",
                success: function (data) {
                    $("#task_key_groups_ids .message").hide();
                    $("#choose_key_word_groups option[value="+value+"]").remove();
                    $("#task_key_groups_ids tbody").append('<tr class="key_word_group"><td><input name="task[task_key_groups_ids]['+data[0]+']" id="task_key_groups_ids_'+data[0]+'" type="hidden" value="'+data[0]+'"><a target="_blank" href="/key_word_groups/'+data[0]+'/edit">'+data[1]+'</a></td><td class="key_words"></td><td><div id="remove_key_word" class="btn red_btn">Удалить из списка</div></td></td></tr>');
                    list = $("#task_key_groups_ids tbody .key_word_group:last-child .key_words").append("<ul class='table_list'></ul>");
                    data[2].forEach(function(item, i, arr) {
                        list.children(".table_list").append("<li><a href='#'>"+item[1]+"</a></li>");
                    });
                }
            });
        } else {

        }
    });
    $("body").on("click", "#remove_key_word", function(){
        value = $(this).parent().parent().children("td:first-child").children(".key_word_group_id").val();
        name = $(this).parent().parent().children("td:first-child").children("a").text();
        $(this).parent().parent().remove();
        $("#choose_key_word_groups").append($("<option></option>")
            .attr("value",value)
            .text(name));
        if ($("#task_key_groups_ids .key_word_group").length == 1) {
            $("#task_key_groups_ids .message").show();
        }
    });
}
*/

/*
function initSubtaskArticles() {
    $("#add_article").click(function(){
        if ($("#subtasks_article_ids .article").length == 0) {
            value = $("#choose_articles").val();
            $.ajax({
                type: "POST",
                data: {"article_id": value },
                url: "/ajax_get_article",
                success: function (data) {
                    $("#subtasks_article_ids .message").hide();
                    $("#choose_articles option[value="+value+"]").remove();
                    $("#subtasks_article_ids tbody").append('<tr class="article"><td><input name="subtask[subtask_articles_ids]['+data[0]+']" id="subtask_articles_ids'+data[0]+'" type="hidden" value="'+data[0]+'"><a target="_blank" href="/articles/'+data[0]+'/edit">'+data[1]+'</a></td><td class="purchase_links"></td><td><div id="remove_purchase_link" class="btn red_btn">Удалить из списка</div></td></td></tr>');
                    list = $("#subtasks_article_ids tbody .article:last-child .purchase_links").append("<ul class='table_list'></ul>");
                    data[2].forEach(function(item, i, arr) {
                        list.children(".table_list").append("<li><a href='#'>"+item[1]+"</a></li>");
                    });
                }
            });
        } else {

        }
    });
    $("body").on("click", "#remove_article", function(){
        value = $(this).parent().parent().children("td:first-child").children(".article_id").val();
        name = $(this).parent().parent().children("td:first-child").children("a").text();
        $(this).parent().parent().remove();
        $("#choose_articles").append($("<option></option>")
            .attr("value",value)
            .text(name));
        if ($("#subtask_articles_ids .article").length == 1) {
            $("#subtask_articles_ids .message").show();
        }
    });
}
*/

/*
function initTaskCommentAdd() {
    if ($("#add_comment_btn").length != 0) {
        $("#add_comment_btn").click(function () {
            if ($("#new_comment_text").val() != "") {
                $.ajax({
                    type: "POST",
                    data: {"task_id": $("#task_id").val(), "text": $("#new_comment_text").val()},
                    url: "/ajax_add_new_comment",
                    success: function (data) {
                        $("#new_comment_text").val("");
                        refreshComments();
                    }
                });
            }
        })
    }
}
*/

/*
function refreshComments() {
    $.ajax({
        type: "POST",
        data: {"task_id": $("#task_id").val()},
        url: "/ajax_get_comments_by_task_id",
        success: function (data) {
            $(".task_comments .comment").remove();
            data.forEach(function(item, i, arr) {
                comment = "<li class='comment'>" +
                    "<div class='img'>" +
                    "<img src='"+item[4]+"'>" +
                    "<div class='name'>"+item[3]+"</div>" +
                    "</div>" +
                    "<div class='texts'>" +
                    "<div class='comment_date'>"+item[1]+"</div>" +
                    "<div class='comment_text'>"+item[0]+"</div>" +
                    "</div>" +
                    "</li>";
                $(".task_comments").append(comment);
            })
        }
    });
}



function addPurchaseLinkToArticle() {
    if ($("#add_purchase_link_to_article").length != 0) {
        $("#add_purchase_link_to_article").click(function(){
            name = $("#choose_link_name").val();
            link = $("#choose_link_link").val();
            price = $("#choose_link_price").val();
            comment = $("#choose_link_comment").val();
            id = randomNumber();
            if ((name != "") && (link != "") && (price != "") && (comment != "")) {
                record = '<tr class="purchase_link">' +
                '<td><input id="purchase_link_id" name="purchase_links['+id+'][name]" type="hidden" value="'+name+'">'+name+'</td>' +
                '<td><input id="purchase_link_link" name="purchase_links['+id+'][link]" type="hidden" value="'+link+'">'+link+'</td>' +
                '<td><input id="purchase_link_price" name="purchase_links['+id+'][price]" type="hidden" value="'+price+'">'+price+'</td>' +
                '<td><input id="purchase_link_comment" name="purchase_links['+id+'][comment]" type="hidden" value="'+comment+'">'+comment+'</td>' +
                '<td><div class="btn red_btn remove_purchase_link">Убрать</div></td>' +
                '</tr>';
                $("#purchase_links .message").remove();
                $("#purchase_links table tbody").append(record);
            }
        });
    }
}

function removePurchaseLink() {
    if ($(".remove_purchase_link").length != 0) {
        $("body").on("click", ".remove_purchase_link", function() {
            $(this).parent().parent().remove();
        });
    }
}

*/

function initCardNav() {
    $(".card_menu li").click(function(){
        index = $(this).index();
        $(".card_menu li").removeClass("blue_btn").addClass("grey_btn");
        $(this).toggleClass("grey_btn").toggleClass("blue_btn");
        $(".card_cnt li.cnt").removeClass("active");
        $($(".card_cnt li.cnt")[index]).addClass("active");
        if(($(this).index() == 3)) {
            $("input[type=submit]").hide();
        } else {
            $("input[type=submit]").show();
        }
    });
}

function initializeHelpBtn() {
    $(".help_container .help_btn").click(function(){
        box = $($(this).parent().children(".box"));
        console.log(box);
        if (box.is(":visible"))
            box.hide();
        else
            box.show();
    });
}

// Done: Название функции в стиле CamelCase
function setCurrentDateToObject( object ) {     // вставка текущей даты в интпут, для переданного объекта
    var date        = new Date(),
        day         = date.getDate(),
        month       = date.getMonth();

    if (object.length) { // коректировка дня и месяца текущей даты. Например 9.9.2015 => 09.09.2015
        if (day < 10) {day = "0" + day;}
        if (month < 10) {month = "0" + month;}

        object.val(day + "." + month + "." + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes());
    }
}

//= require turbolinks



