var send_flag = false;

function addCommentInTask() {
    button = $("#add_comment_btn");
    if (button.length != 0) {
        button.click(function () {
            send_flag = true;
            new_comment_text = $("#new_comment_text");
            task_id = $("#task_id");
            if (new_comment_text.val() != "") {
                $.ajax({
                    type: "POST",
                    data: {"task_id": task_id.val(), "text": new_comment_text.val()},
                    url: "/ajax_add_new_comment",
                    success: function (data) {
                        new_comment_text.val("");
                        refreshCommentsInTask();
                        send_flag = false;
                    }
                });
            }
        });
    }
}

function refreshCommentsInTask() {
    task_id = $("#task_id");
    task_comments = $(".task_comments");
    task_comment_class = ".comment";

    $.ajax({
        type: "POST",
        data: { "task_id": task_id.val() },
        url: "/ajax_get_comments_by_task_id",
        success: function (data) {
            $(task_comments.find(task_comment_class)).remove();
            data.forEach(function(item, i, arr) {
                task_comments.append(generateCommentRow(item[0], item[1], item[3], item[4]));
            })
        }
    });
}

function generateCommentRow(comment_text, comment_date, user_name, user_image) {
    return "<li class='comment'>" +
            "<div class='img'>" +
                "<img src='" + user_image + "'>" +
                "<div class='name'>" + user_name + "</div>" +
            "</div>" +
            "<div class='texts'>" +
                "<div class='comment_date'>" + comment_date + "</div>" +
                "<div class='comment_text'>" + comment_text + "</div>" +
            "</div>" +
        "</li>";
}

