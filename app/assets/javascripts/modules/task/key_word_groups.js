var send_flag = false;

function initializeActionWithKeyWordsGroupsInTask() {
    AddKeyWordsGroupToTask();
    RemoveKeyWordsGroupToTask();
}

function AddKeyWordsGroupToTask() {
    button = $("#add_key_words");
    button.click(function(){
        if (!send_flag) {
            send_flag = true;
            choose_key_word_groups = $("#choose_key_word_groups");
            choose_key_word_groups_val = choose_key_word_groups.val();
            task_key_groups_ids = $("#task_key_groups_ids");

            $.ajax({
                type: "POST",
                data: { "group_id": choose_key_word_groups_val },
                url: "/ajax_get_key_word_group",
                success: function (data) {
                    send_flag = false;
                    $(task_key_groups_ids.find(".message")).hide();
                    $($(choose_key_word_groups).find("option[value="+choose_key_word_groups_val+"]")).remove();
                    $($(task_key_groups_ids).find("tbody")).append(generateKeyWordGroupRow(data[0], data[1]));
                    key_words_container = $($(task_key_groups_ids).find("tbody .key_word_group:last-child .key_words")).append(generateKeyWordsContainer());

                    data[2].forEach(function(item, i, arr) {
                        $(key_words_container.find(".table_list")).append(generateKeyWordRecord(item[0], item[1]));
                    });

                }
            });
        }
    });
}

function RemoveKeyWordsGroupToTask() {
    button = "#remove_key_word";

    $("body").on("click", button, function(){

        key_word_group_id = ".key_word_group_id";
        key_word_group_ids = $("#key_word_group_ids");
        choose_key_word_groups = $("#choose_key_word_groups");
        key_word_group_class = ".key_word_group";
        message_class = ".message";

        key_word_group_row = $(this)
            .parent()
            .parent()
            .children("td:first-child");
        key_word_group_id_val = key_word_group_row
            .children(key_word_group_id)
            .val();
        key_word_group_name = key_word_group_row
            .children("a")
            .text();
        $(this).parent().parent().remove();
        choose_key_word_groups.append(generateKeyWordOption(key_word_group_id_val, key_word_group_name));

        if (key_word_group_ids.find(key_word_group_class).length == 1)
            key_word_group_ids.find(message_class).show();

    });
}

function generateKeyWordGroupRow(key_word_group_id, key_word_group_name) {
    return '<tr class="key_word_group">' +
        '<td><input class="key_word_group_id" name="task[task_key_groups_ids][' + key_word_group_id + ']" id="task_key_groups_ids_' + key_word_group_id + '" type="hidden" value="' + key_word_group_id + '">' +
        '<a target="_blank" href="/key_word_groups/' + key_word_group_id + '/edit">' + key_word_group_name + '</a>' +
        '</td>' +
        '<td class="key_words"></td>' +
        '<td><div id="remove_key_word" class="btn red_btn">Удалить из списка</div></td>' +
        '</tr>';
}

function generateKeyWordsContainer() {
    return "<ul class='table_list'></ul>";
}

function generateKeyWordRecord(key_word_id, key_word_name) {
    return "<li><a target='_blank' href='/key_words/"+key_word_id+"/edit'>" + key_word_name + "</a></li>";
}

function generateKeyWordOption(key_word_id, key_word_name) {
    return "<option value='"+key_word_id+"'>"+key_word_name+"</option>";
}