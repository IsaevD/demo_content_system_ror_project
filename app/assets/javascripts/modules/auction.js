function initializeSubtasksAuction() {
    if ($("#auction").length != 0) {
        reserveSubtaskInAuction();
        setInterval(function(){ refreshAuctionTable();}, 2000);
    }
}

function reserveSubtaskInAuction() {
    button_class = ".get_subtask";
    $("body").on("click", button_class,function(){

        subtask_id = $(this)
            .parent()
            .parent()
            .attr("data-subtask-id");

        $.ajax({
            type: "POST",
            data: {"subtask_id": subtask_id},
            url: "/ajax_get_subtask_from_auction",
            success: function (data) {
                refreshAuctionTable();
            }
        });

    });
}

function refreshAuctionTable() {
    auction = $("#auction");
    subtask_class = ".subtask";
    subtask_table_class = ".table";

    $.ajax({
        type: "POST",
        url: "/ajax_auction_subtasks",
        success: function (data) {
            $(auction.find(subtask_class)).remove();
            data.forEach(function(item, i, arr) {
                $(auction.find(subtask_table_class)).append(generateAuctionRow(item[0], item[1], item[2], item[3], item[4]));
            })
        }
    });
}

function generateAuctionRow(subtask_id, project_name, subtask_name, text_volume, price ) {
    return  '<tr class="subtask" data-subtask-id="' + subtask_id + '">' +
        '<td>' + project_name + '</td>' +
        '<td>' + subtask_name + '</td>' +
        '<td>' + text_volume + '</td>' +
        '<td>' + price + '</td>' +
        '<td><div class="btn blue_btn get_subtask">Выбрать</div></td>' +
        '</tr>';
}