//= require modules/key_word_group

function initializeActionsWithKeyWordInKeyWordGroup() {
    addNewKeyWordToKeyWordGroup();
    removeKeyWordInKeyWordGroup();
}

function addNewKeyWordToKeyWordGroup() {
    form_cnt = $("#add_key_word");
    button = $("#add_key_words_to_group");
    key_word_entry_count = $("#choose_key_word_entry_count");
    key_word_state_id = $("#choose_key_word_state");
    key_word_state_name = $("#choose_key_word_state");
    key_word_name = $("#choose_key_word_name");

    if (button.length != 0) {
        button.click(function(){

            key_word_fake_id = randomNumber();
            key_word_entry_count_val = key_word_entry_count.val();
            key_word_state_id_val = key_word_state_id.val();
            key_word_state_name_val = key_word_state_name.children("option:selected").text();
            key_word_name_val = key_word_name.val();
            key_word_table_cnt = $("#keywords");

            clearKeyWordsErrors();

            if (key_word_name_val != "") {
                if (key_word_entry_count_val != "") {
                    $(key_word_table_cnt.find(".message")).remove();
                    $($(key_word_table_cnt).find("tbody")).append(
                        generateKeyWordRow(
                            key_word_fake_id,
                            key_word_name_val,
                            key_word_state_id_val,
                            key_word_state_name_val,
                            key_word_entry_count_val
                        ));
                        clearAddKeyWordForm();
                } else {
                    generateKeyWordErrors(key_word_entry_count);
                }
            } else {
                generateKeyWordErrors(key_word_name);
            }
        });
    }

    function clearAddKeyWordForm() {
        key_word_entry_count.val("");
        key_word_name.val("");
    }

    function clearKeyWordsErrors() {
        form_cnt.find(".error").each(function(){
            $(this).text("");
        });
    }
}

function removeKeyWordInKeyWordGroup() {
    button = ".remove_key_word";

    if ($(button).length != 0) {
        $("body").on("click", button, function () {
            $(this).parent().parent().remove();
        });
    }
}

function generateKeyWordRow(id, key_word_name, key_word_state_id, key_word_state_name, key_word_entry_count) {
    var select_tag = $("#choose_key_word_state"),
        select_tag_options = select_tag.find('option'),
        result_options = "";

    $.each(select_tag_options, function ( key , value ) {
        if ($(value).val() == key_word_state_id) {
            result_options += '<option selected value = "' + key_word_state_id + '">' + $(value).html() + '</option>';
        } else {
            result_options += '<option value = "' + $(value).val() + '">' + $(value).html() + '</option>';
        }
    });

    return '<tr class="key_word">' +
            '<td><input id="key_word_id" name="key_words['+id+'][name]" type="text" value="'+key_word_name+'"></td>' +
            '<td><input id="entry" name="key_words['+id+'][entry_count]" type="text" value="'+key_word_entry_count+'"></td>' +
            '<td><select id="key_words_'+id+'_state_id" name="key_words['+id+'][state_id]">'+result_options+'<select></td>'+
            '<td><div class="btn red_btn remove_key_word">Убрать</div></td>' +
        '</tr>';
}

function generateKeyWordErrors(field) {
    $($(field).parent().find(".error")).text("Не заполнено обязательное поле");
}