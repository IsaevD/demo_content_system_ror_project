//= require modules/key_word_group

function initializeActionsWithPurchaseLinksInArticle() {
    addNewPurchaseLinkToArticle();
    removePurchaseLinkInArticle();
}

function addNewPurchaseLinkToArticle() {

    form_cnt = $("#add_purchase_links");
    button = $("#add_purchase_link_to_article");
    choose_link_name = $("#choose_link_name");
    choose_link_link = $("#choose_link_link");
    choose_link_price = $("#choose_link_price");
    choose_link_comment = $("#choose_link_comment");

    if (button.length != 0) {
        button.click(function(){

            link_fake_id = randomNumber();
            link_name_val = choose_link_name.val();
            link_link_val = choose_link_link.val();
            link_price_val = choose_link_price.val();
            link_comment_val = choose_link_comment.val();
            link_table_cnt = $("#purchase_links");

            clearLinksErrors();

            if (link_name_val != "") {
                if (link_link_val != "") {
                    if (link_price_val != "") {
                        if (link_comment_val != "") {
                            $(link_table_cnt.find(".message")).remove();
                            $($(link_table_cnt).find("tbody")).append(
                                generatePurchaseLinkRow(
                                    link_fake_id,
                                    link_name_val,
                                    link_link_val,
                                    link_price_val,
                                    link_comment_val
                                ));
                            clearAddLinkForm();
                        }
                        else {
                            generateLinkErrors(choose_link_comment);
                        }
                    } else {
                        generateLinkErrors(choose_link_price);
                    }
                } else {
                    generateLinkErrors(choose_link_link);
                }
            } else {
                generateLinkErrors(choose_link_name);
            }
        });
    }

    function clearAddLinkForm() {
        choose_link_name.val("");
        choose_link_link.val("");
        choose_link_price.val("");
        choose_link_comment.val("");
    }

    function clearLinksErrors() {
        form_cnt.find(".error").each(function(){
            $(this).text("");
        });
    }
}

function removePurchaseLinkInArticle() {
    button = ".remove_key_word";

    if ($(button).length != 0) {
        $("body").on("click", button, function () {
            $(this).parent().parent().remove();
        });
    }
}

function generatePurchaseLinkRow(id, link_name, link_link, link_price, link_comment) {
    return '<tr class="purchase_link">' +
        '<td><input id="purchase_link_id" name="purchase_links['+id+'][name]" type="hidden" value="' + link_name + '">' + link_name + '</td>' +
        '<td><input id="purchase_link_link" name="purchase_links['+id+'][link]" type="hidden" value="' + link_link + '">' + link_link + '</td>' +
        '<td><input id="purchase_link_price" name="purchase_links['+id+'][price]" type="hidden" value="' + link_price + '">' + link_price + '</td>' +
        '<td><input id="purchase_link_comment" name="purchase_links['+id+'][comment]" type="hidden" value="' + link_comment + '">'+ link_comment + '</td>' +
        '<td><div class="btn red_btn remove_purchase_link">Убрать</div></td>' +
        '</tr>';
}

function generateLinkErrors(field) {
    $($(field).parent().find(".custom_error")).text("Не заполнено обязательное поле");
}