var send_flag = false;

function addCommentInSubtask() {
    button = $("#add_subtask_comment_btn");
    if (button.length != 0) {
        button.click(function () {
            send_flag = true;
            new_comment_text = $("#new_comment_text");
            subtask_id = $("#subtask_id");
            if (new_comment_text.val() != "") {
                $.ajax({
                    type: "POST",
                    data: {"subtask_id": subtask_id.val(), "text": new_comment_text.val()},
                    url: "/ajax_add_new_comment_to_subtask",
                    success: function (data) {
                        new_comment_text.val("");
                        refreshCommentsInSubtask();
                        send_flag = false;
                    }
                });
            }
        });
    }
}

function refreshCommentsInSubtask() {
    subtask_id = $("#subtask_id");
    task_comments = $(".task_comments");
    task_comment_class = ".comment";

    $.ajax({
        type: "POST",
        data: { "subtask_id": subtask_id.val() },
        url: "/ajax_get_comments_by_subtask_id",
        success: function (data) {
            $(task_comments.find(task_comment_class)).remove();
            data.forEach(function(item, i, arr) {
                task_comments.append(generateSubtaskCommentRow(item[0], item[1], item[3], item[4]));
            })
        }
    });
}

function generateSubtaskCommentRow(comment_text, comment_date, user_name, user_image) {
    return "<li class='comment'>" +
            "<div class='img'>" +
                "<img src='" + user_image + "'>" +
                "<div class='name'>" + user_name + "</div>" +
            "</div>" +
            "<div class='texts'>" +
                "<div class='comment_date'>" + comment_date + "</div>" +
                "<div class='comment_text'>" + comment_text + "</div>" +
            "</div>" +
        "</li>";
}

