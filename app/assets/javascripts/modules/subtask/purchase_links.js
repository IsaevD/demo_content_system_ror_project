var send_flag = false;

function initializeActionWithArticlesInSubtask() {
    AddArticleToSubtask();
    RemoveArticleFromTask();
}

function AddArticleToSubtask() {
    button = $("#add_subtask_articles");
    button.click(function(){
        if (!send_flag) {
            send_flag = true;
            choose_purchase_link = $("#choose_purchase_links");
            choose_purchase_link_val = choose_purchase_link.val();
            subtask_purchase_links_ids = $("#subtask_purchase__links_ids");
            if (choose_purchase_link_val == "") {
                send_flag = false;
                return;
            }
            $.ajax({
                type: "POST",
                data: { "purchaseLink_id": choose_purchase_link_val },
                url: "/ajax_get_purchase_link",
                success: function (data) {
                    send_flag = false;
                    $(subtask_purchase_links_ids.find(".message")).hide();
                    $($(choose_purchase_link).find("option[value="+choose_purchase_link_val+"]")).remove();
                    $($(subtask_purchase_links_ids).find("tbody")).append(generateArticleRow(data[0], data[1]));
                    articles_container = $($(subtask_purchase_links_ids).find("tbody .purchase_links:last-child .purchase_link")).append(generatePurchaseLinksContainer());
//                    data[2].forEach(function(item, i, arr) {
//                        $(articles_container.find(".table_list")).append(generatePurchaseLinkRecord(item[0], item[1]));
//                    });

                }
            });
        }
    });
}

function RemoveArticleFromTask() {
    button = ".remove_link";

    $("body").on("click", button, function(){

        pLink_id = ".purchase_link_id";
//        article_ids = "#purchase_link_ids";
        choose_pLinks = $("#choose_purchase_links");
        pLink_class = ".purchase_link_id";
        message_class = ".message";

        pLink_row = $(this)
            .parent()
            .parent()
            .children("td:first-child");
        pLink_id_val = pLink_row
            .children(pLink_id)
            .val();
        pLink_name = pLink_row
            .children("a")
            .text();


        $(this).parent().parent().remove();
        $('select#choose_purchase_links').append('<option value = "' + pLink_id_val + '">' + pLink_name + '</option>');
        choose_articles.append(generatePurchaseLinkOption(pLink_id_val, pLink_name));

//        if (article_ids.find(article_class).length == 1)
//            article_ids.find(message_class).show();

    });
}

function generateArticleRow(purchase_link_id, purchase_link_name) {
    return '<tr class="purchase_link">' +
        '<td><input class="purchase_link_id" name="subtask[subtask_purchase_link_ids][' + purchase_link_id + ']" id="subtask_purchase_link_ids_' + purchase_link_id + '" type="hidden" value="' + purchase_link_id + '">' +
        '<a target="_blank" href="/purchase_links/' + purchase_link_id + '/edit">' + purchase_link_name + '</a>' +
        '</td>' +
//        '<td class="purchase_links"></td>' +
        '<td><div class="remove_link btn red_btn">Удалить из списка</div></td>' +
        '</tr>';
}

function generatePurchaseLinksContainer() {
    return "<ul class='table_list'></ul>";
}

function generatePurchaseLinkRecord(purchase_link_id, purchase_link_name) {
    return "<li><a target='_blank' href='/purchase_links/" + purchase_link_id + "/edit'>" + purchase_link_name + "</a></li>";
}

function generatePurchaseLinkOption(purchase_link_id, purchase_link_name) {
    return "<option value='" + purchase_link_id + "'>" + purchase_link_name + "</option>";
}