function initializeForms() {
    initializeKeyWordGroupForm();
    initializeKeyWordGroupList();
    initializeKeyWordForm();
    initializeTaskForKeyManager();
    initializePurchaseLinkForm();
}

function initializeKeyWordGroupForm() {
    numeric_elements = ["#key_word_group_volume_to", "#key_word_group_volume_of", "#choose_key_word_entry_count", "#key_word_entry"];
    initNumber(numeric_elements);
}

function initializeKeyWordGroupList() {
    numeric_elements = ["#filter_text_volume_to", "#filter_text_volume_of"];
    initNumber(numeric_elements);
}

function initializeKeyWordForm() {
    numeric_elements = ["#key_word_entry_count"];
    initNumber(numeric_elements);
}

function initializePurchaseLinkForm() {
    numeric_elements = ["#purchase_link_price"];
    initNumber(numeric_elements);
}

function initializeTaskForKeyManager() {
    disabled_elements = ["#task_date"];
    if ($(".key_manager_task").length != 0)
        initDisabled(disabled_elements);
}


function initNumber(object_arr) {
    object_arr.forEach(function(element) {
        $(element).numberMask({beforePoint:8});
    });
}

function initDisabled(object_arr) {
    object_arr.forEach(function(element) {
        $(element).attr("disabled", true);
        $(element).addClass("disabled");
    });
}