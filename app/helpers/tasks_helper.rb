module TasksHelper

  def change_task_stage(task)
    case task.task_stage.alias
      when TaskStage.INITIALIZE_ALIAS
        case task.task_state.alias
          when TaskState.COMPLETE_ALIAS
            task.task_state = TaskState.in_progress_state
            task.task_stage = TaskStage.key_manager_work_stage
        end
      when TaskStage.KEY_MANAGER_WORK_ALIAS
        case task.task_state.alias
          when TaskState.COMPLETE_ALIAS
            generate_subtasks(task)
            task.task_state = TaskState.in_progress_state
            task.task_stage = TaskStage.content_and_link_manager_stage
          when TaskState.FOR_REVISION_ALIAS
            task.task_state = TaskState.in_progress_state
            task.task_stage = TaskStage.initialize_stage
        end
      when TaskStage.CONTENT_AND_LINK_MANAGER_STAGES_ALIAS
      when TaskStage.COMPLETED_ALIAS
    end

    task.save

    return task
  end

  def change_subtask_stage(subtask)
    case subtask.subtask_stage.alias
      when SubtaskStage.INITIALIZE_STAGE_ALIAS
        case subtask.task_state.alias
          when TaskState.COMPLETE_ALIAS
            subtask.task_state = TaskState.in_progress_state
            subtask.subtask_stage = SubtaskStage.auction_content_manager_work_stage
        end
      when SubtaskStage.AUCTION_CONTENT_MANAGER_WORK_STAGE_ALIAS
      when SubtaskStage.CONTENT_MANAGER_WORK_STAGE_ALIAS
        case subtask.task_state.alias
          when TaskState.FOR_REVISION_ALIAS
            subtask.task_state = TaskState.in_progress_state
            subtask.subtask_stage = SubtaskStage.initialize_stage
          when TaskState.COMPLETE_ALIAS
            subtask.task_state = TaskState.in_progress_state
            subtask.subtask_stage = SubtaskStage.link_manager_work_stage
        end
      when SubtaskStage.LINK_MANAGER_WORK_STAGE_ALIAS
        case subtask.task_state.alias
          when TaskState.FOR_REVISION_ALIAS
            subtask.task_state = TaskState.in_progress_state
            subtask.subtask_stage = SubtaskStage.content_manager_work_stage
          when TaskStage.COMPLETED_ALIAS
            subtask.task_state = TaskState.complete_state
            subtask.subtask_stage = SubtaskStage.completed_stage
        end
      when TaskStage.COMPLETED_ALIAS
    end
    subtask.save

    return subtask

  end

  def generate_subtasks(task)
    task.key_word_groups.each do |key_word_group|
      subtask = Subtask.new
      subtask.name = key_word_group.name
      subtask.date = task.date
      subtask.description = key_word_group.description
      subtask.key_word_group_id = key_word_group.id
      subtask.task_state = TaskState.in_progress_state
      subtask.task_id = task.id
      subtask.link_manager_id = task.link_manager_id
      subtask.task_state_id = TaskState.in_progress_state.id
      subtask.subtask_stage = SubtaskStage.auction_content_manager_work_stage
      subtask.save
    end
  end

end
