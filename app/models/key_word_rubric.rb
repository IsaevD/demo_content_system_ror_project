class KeyWordRubric < ActiveRecord::Base

  VALID_ALIAS_REGEX = /[a-zA-Z]+/

  validates :alias,
      presence: true,
      length: { minimum: 4, maximum: 50},
      uniqueness: true,
      format: { with: VALID_ALIAS_REGEX }

  validates :name,
      presence: true,
      uniqueness: true

end
