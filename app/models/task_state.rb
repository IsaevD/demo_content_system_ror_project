class TaskState < ActiveRecord::Base

  validates :alias,
    presence: true

  validates :name,
    presence: true

  def self.IN_PROGRESS_ALIAS
    return "in_progress"
  end
  def self.FOR_REVISION_ALIAS
    return "for_revision"
  end
  def self.COMPLETE_ALIAS
    return "complete"
  end

  def self.in_progress_state
    return self.find_by(:alias => self.IN_PROGRESS_ALIAS)
  end
  def self.for_revision_state
    return self.find_by(:alias => self.FOR_REVISION_ALIAS)
  end
  def self.complete_state
    return self.find_by(:alias => self.COMPLETE_ALIAS)
  end


end
