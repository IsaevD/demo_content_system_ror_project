class TaskStage < ActiveRecord::Base

  validates :alias,
            presence: true

  validates :name,
            presence: true

  def self.INITIALIZE_ALIAS
    return "initialize"
  end
  def self.KEY_MANAGER_WORK_ALIAS
    return "key_manager_work_stage"
  end
  def self.CONTENT_AND_LINK_MANAGER_STAGES_ALIAS
    return "content_and_link_manager_work_stage"
  end
  def self.COMPLETED_ALIAS
    return "completed"
  end

  def self.initialize_stage
    return self.find_by(:alias => self.INITIALIZE_ALIAS)
  end
  def self.key_manager_work_stage
    return self.find_by(:alias => self.KEY_MANAGER_WORK_ALIAS)
  end
  def self.content_and_link_manager_stage
    return self.find_by(:alias => self.CONTENT_AND_LINK_MANAGER_STAGES_ALIAS)
  end
  def self.completed_stage
    return self.find_by(:alias => self.COMPLETED_ALIAS)
  end

end
