module SystemMainItemsD
  class SystemUserRole < ActiveRecord::Base
    belongs_to :system_state

    VALID_ALIAS_REGEX = /[a-zA-Z]+/

    validates :alias,
              presence: true,
              length: { minimum: 4, maximum: 50},
              uniqueness: true,
              format: { with: VALID_ALIAS_REGEX }

    validates :name,
              presence: true,
              length: { minimum: 4, maximum: 255},
              uniqueness: true

    # validates :rate,
    #           presence: true

    def self.ROOT_ROLE_ALIAS
      return "root"
    end
    def self.KEY_MANAGER_ALIAS
      return "key_manager"
    end
    def self.LINK_MANAGER_ALIAS
      return "link_manager"
    end
    def self.CONTENT_MANAGER_ALIAS
      return "content_manager"
    end

    def self.root_role
      return self.find_by(:alias => self.ROOT_ROLE_ALIAS)
    end
    def self.key_manager_role
      return self.find_by(:alias => self.KEY_MANAGER_ALIAS)
    end
    def self.link_manager_role
      return self.find_by(:alias => self.LINK_MANAGER_ALIAS)
    end
    def self.content_manager_role
      return self.find_by(:alias => self.CONTENT_MANAGER_ALIAS)
    end

  end
end
