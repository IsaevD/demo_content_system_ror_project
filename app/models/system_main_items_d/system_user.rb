module SystemMainItemsD
  class SystemUser < ActiveRecord::Base

    belongs_to :system_user_role
    belongs_to :system_state

    has_attached_file :avatar, :styles => { :thumb => "100x100" }, :default_url => "/images/system_main_items/no_user_avatar.png"

    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

    validates :login,
              presence: true,
              length: { minimum: 3, maximum: 50 },
              uniqueness: true

    validates :email,
              presence: true,
              length: { minimum: 5, maximum: 255},
              uniqueness: true,
              format: { with: VALID_EMAIL_REGEX }

    validates :rate,
              presence: true

    validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

    has_secure_password

    def self.get_key_managers
      #return self.find_by(:alias => , :system_state => SystemMainItemsD::SystemState.active_state_id).all
    end
    def self.get_link_managers
      #return self.find_by(:alias => self.link_manager_alias, :system_state => SystemMainItemsD::SystemState.active_state_id).all
    end
    def self.get_content_managers
      #return self.find_by(:alias => self.link_content_alias, :system_state => SystemMainItemsD::SystemState.active_state_id).all
    end

  end
end
