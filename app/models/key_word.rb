class KeyWord < ActiveRecord::Base

  belongs_to :key_word_group
  belongs_to :key_word_state

  validates :name,
      presence: true,
      uniqueness: true

  validates :entry_count,
      presence: false

end
