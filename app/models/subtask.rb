class Subtask < ActiveRecord::Base
  belongs_to :task_state
  belongs_to :subtask_stage
  belongs_to :task
  belongs_to :key_word_group
  has_many :articles
  has_many :comments
  has_many :purchase_links

  def self.get_subtasks_by_content_manager(system_user)
    return self.where(:content_manager_id => system_user.id, :subtask_stage_id => SubtaskStage.content_manager_work_stage.id)
  end

  def self.get_subtasks_by_link_manager(system_user)
    return self.where(:link_manager_id => system_user.id, :subtask_stage_id => SubtaskStage.link_manager_work_stage.id)
  end

  def self.get_auction_subtasks_by_content_manager(system_user)
    return_subtasks = []
    self.where(:subtask_stage_id => SubtaskStage.auction_content_manager_work_stage.id).each do |subtask|
      task_id = subtask.task_id
      content_managers = TaskContentManager.where(:task_id => task_id)
      if !content_managers.find_by(:system_user_id => system_user.id).nil?
        return_subtasks << subtask
      end
    end
    return return_subtasks
  end

end
