class Project < ActiveRecord::Base
  
  # toDO: Добавить валидацию URI
  VALID_URL_REGEX = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/i

  validates :name,
      presence: true,
      uniqueness: true,
      length: { minimum: 4, maximum: 255}

  validates :url,
      presence: true,
      uniqueness: true

end
