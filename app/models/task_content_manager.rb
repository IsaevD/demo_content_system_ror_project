class TaskContentManager < ActiveRecord::Base

  def self.get_content_managers_by_task_id(task_id)
    return TaskContentManager.where(:task_id => task_id)
  end

end
