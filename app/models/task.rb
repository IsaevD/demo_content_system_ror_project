class Task < ActiveRecord::Base

  has_many :key_word_groups
  has_many :comments
  belongs_to :project
  belongs_to :task_stage
  belongs_to :task_state
  has_many :subtasks

  validates :name,
    presence: true

  validates :description,
    presence: true

  validates :date,
    presence: true

  def self.get_task_project_name(task)
    if !task.project.nil?
      return task.project.name
    else
      return "-"
    end
  end

  def self.get_task_stage_name(task)
    if !task.task_stage.nil?
      return task.task_stage.name
    else
      return "-"
    end
  end


end
