class KeyWordGroup < ActiveRecord::Base

  has_many :key_words
  belongs_to :key_word_rubric

  validates :name,
      presence: true

  validates :volume_of,
      presence: true

  validates :volume_to,
      presence: true

  validates :exact_entry,
      presence: true

end