class SubtaskStage < ActiveRecord::Base

  def self.INITIALIZE_STAGE_ALIAS
    return "initialize_stage"
  end

  def self.AUCTION_CONTENT_MANAGER_WORK_STAGE_ALIAS
    return "auction_content_manager_work_stage"
  end

  def self.CONTENT_MANAGER_WORK_STAGE_ALIAS
    return "content_manager_work_stage"
  end

  def self.LINK_MANAGER_WORK_STAGE_ALIAS
    return "link_manager_work_stage"
  end

  def self.COMPLETED_STAGE_ALIAS
    return "completed"
  end

  def self.initialize_stage
    return self.find_by(:alias => self.INITIALIZE_STAGE_ALIAS)
  end

  def self.auction_content_manager_work_stage
    return self.find_by(:alias => self.AUCTION_CONTENT_MANAGER_WORK_STAGE_ALIAS)
  end

  def self.content_manager_work_stage
    return self.find_by(:alias => self.CONTENT_MANAGER_WORK_STAGE_ALIAS)
  end

  def self.link_manager_work_stage
    return self.find_by(:alias => self.LINK_MANAGER_WORK_STAGE_ALIAS)
  end

  def self.completed_stage
    return self.find_by(:alias => self.COMPLETED_STAGE_ALIAS)
  end

end
