module SystemMainItemsD
  class SystemUserRolesController < ApplicationController
    before_filter :init_variables

    def index
      all_item(@entity, nil, true, true, true)
    end

    def show
      get_item_by_id(@entity, params[:id], true)
    end

    def new
      new_item(@entity)
    end

    def edit
      get_item_by_id(@entity, params[:id], true)
    end

    def create
      create_item(@entity, @path, true)
    end

    def update
      update_item(@entity, @path, params[:id], true)
    end

    def destroy
      delete_item(@entity, @path, params[:id], true)
    end

    private

      def init_variables

        @object = {
            :name => "system_user_roles",
            :entity => SystemMainItemsD::SystemUserRole,
            :param_name => :system_user_role,
            :paths => {
                :all_path => system_main_items_d.system_user_roles_path,
                :new_path => system_main_items_d.new_system_user_role_path,
                :edit_path => system_main_items_d.system_user_roles_path
            },
            :breadcrumbs => {
                t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
                t("modules.system_user_roles.name") => nil
            },
            :fields => {
                :alias =>  {
                    :type => :string,
                    :label => t('form.labels.alias'),
                    :show_in_table => true,
                    :show_in_card => true,
                    :required => true
                },
                :name =>  {
                    :type => :string,
                    :label => t('form.labels.name'),
                    :show_in_table => true,
                    :show_in_card => true,
                    :required => true
                },
                :description =>  {
                    :type => :text,
                    :label => t('form.labels.description'),
                    :show_in_table => false,
                    :show_in_card => true
                },
                :rate =>  {
                    :type => :string,
                    :label => t('form.labels.rate'),
                    :show_in_table => true,
                    :show_in_card => true,
                    :required => true
                },
                :system_state_id =>  {
                    :type => :collection,
                    :label => t('form.labels.system_state_id'),
                    :show_in_table => false,
                    :show_in_card => true,
                    :where_entity => SystemMainItemsD::SystemState,
                    :where_visible_field => :name,
                    :where_statement => nil,
                    :settings => {
                      :include_blank => false
                    }
                }
            }
        }
        @fields = @object[:fields]
        @visible_fields = []
        @fields.each do |key, value|
          if value[:show_in_table]
            @visible_fields << key
          end
        end
        @entity = @object[:entity]
        @param_name = @object[:param_name]
        @path = @object[:paths][:all_path]
        @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
      end

  end
end
