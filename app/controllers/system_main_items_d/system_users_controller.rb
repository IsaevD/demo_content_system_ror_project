module SystemMainItemsD
  class SystemUsersController < ApplicationController
    before_filter :init_variables

    def index
      all_item(@entity, params[:filter], true, true, true)
    end

    def show
      get_item_by_id(@entity, params[:id], true)
    end

    def new
      new_item(@entity)
    end

    def edit
      get_item_by_id(@entity, params[:id], true)
    end

    def create
      create_item(@entity, @path, true)
    end

    def update
      update_item(@entity, @path, params[:id], true)
    end

    def destroy
      delete_item(@entity, @path, params[:id], true)
    end

    private

      def init_variables

        @object = {
            :name => "system_users",
            :entity => SystemMainItemsD::SystemUser,
            :param_name => :system_user,
            :paths => {
                :all_path => system_main_items_d.system_users_path,
                :new_path => system_main_items_d.new_system_user_path,
                :edit_path => system_main_items_d.system_users_path
            },
            :breadcrumbs => {
                t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
                t("modules.system_users.name") => nil
            },
            :fields => {
                :login => {
                    :type => :string,
                    :label => t('form.labels.login'),
                    :show_in_card => true,
                    :show_in_table => true,
                    :required => true
                },
                :email => {
                    :type => :string,
                    :label => t('form.labels.email'),
                    :show_in_card => true,
                    :show_in_table => true,
                    :required => true
                },
                :rate =>  {
                    :type => :string,
                    :label => t('form.labels.rate'),
                    :show_in_table => true,
                    :show_in_card => true,
                    :required => true
                },
                :system_user_role_id => {
                    :type => :collection,
                    :label => t('form.labels.system_user_role_id'),
                    :show_in_card => true,
                    :show_in_table => true,
                    :where_entity => SystemMainItemsD::SystemUserRole,
                    :where_visible_field => :name,
                    :where_statement => nil,
                    :settings => {
                        :include_blank => false
                    }
                },
                :system_state_id =>  {
                    :type => :collection,
                    :label => t('form.labels.system_state_id'),
                    :show_in_table => false,
                    :show_in_card => true,
                    :where_entity => SystemMainItemsD::SystemState,
                    :where_visible_field => :name,
                    :where_statement => nil,
                    :settings => {
                        :include_blank => false
                    }
                },
                :personal_data => {
                    :type => :group_label,
                    :label => t("custom.labels.personal_data")
                },
                :first_name => {
                    :type => :string,
                    :label => t('form.labels.first_name'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :middle_name => {
                    :type => :string,
                    :label => t('form.labels.middle_name'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :last_name => {
                    :type => :string,
                    :label => t('form.labels.last_name'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :avatar => {
                    :type => :image,
                    :label => t('form.labels.avatar'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :label => {
                    :type => :group_label,
                    :label => t("custom.labels.change_password")
                },
                :password => {
                    :type => :string,
                    :label => t('form.labels.password'),
                    :show_in_card => true,
                    :show_in_table => false
                },
                :password_confirmation => {
                    :type => :string,
                    :label => t('form.labels.password_confirmation'),
                    :show_in_card => true,
                    :show_in_table => false
                }
            }
        }
        @fields = @object[:fields]
        @visible_fields = []
        @fields.each do |key, value|
          if value[:show_in_table]
            @visible_fields << key
          end
        end
        @entity = @object[:entity]
        @param_name = @object[:param_name]
        @path = @object[:paths][:all_path]
        @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
      end

  end
end
