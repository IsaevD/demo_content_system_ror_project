class AccessRightsD::ApplicationController < Admin::ApplicationController
  skip_before_filter :check_access_rights, :only => [:no_access_rights]
end