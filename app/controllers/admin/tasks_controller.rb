class Admin::TasksController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    path = @path

    is_log = true
    is_redirect = true
    #toDO: Добавить проверку существования контент-менеджеров
    @item = @entity.new(prepared_params(@param_name, @params_array))
    if @item.save
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :dual_list
            if settings[:show_in_card]
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], @item.id)
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
      end
      if (is_redirect)

        # Обновление ключевых слов
        KeyWordGroup.where(:task_id => @item.id).each do |group|
          group.task_id = nil
          group.save
        end
        if !params[@param_name][:task_key_groups_ids].nil?
          params[@param_name][:task_key_groups_ids].each do |key, value|
            group = KeyWordGroup.find(key)
            group.task_id = @item.id
            group.save
          end
        end

        change_task_stage(@item)

        redirect_to path
      end
    else
      render "new"
    end

    #create_item(@entity, @path, true)
  end

  def update

    is_log = true
    id = params[:id]
    @item = @entity.find(params[:id])

    @fields.each do |field, settings|
      entity_params = params[@param_name]
      case settings[:type]
        when :image
          if entity_params[field] == "nil"
            params[@param_name][field] = nil
          end
        when :dual_list
          if settings[:show_in_card]
            current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
            current_values.destroy_all
            values = entity_params[field]
            if !values.nil?
              values.each do |key, value|
                settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
              end
            end
          end
        when :image_list
          if !entity_params[settings[:image_entity_field]].nil?
            entity_params[settings[:image_entity_field]].each do |key, value|
              if value == "nil"
                @fields[field][:image_entity].find(key).destroy
              end
            end
          end
          if !entity_params[field].nil?
            entity_params[field].each do |value|
              image_list = @fields[field][:image_entity].new
              image_list.update_attribute(@fields[field][:entity_field], params[:id])
              image_list.update_attribute(@fields[field][:image_entity_field], value)
              image_list.save
            end
          end
      end
    end

    if @item.update_attributes(prepared_params(@param_name, @params_array))
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
      end

      # Обновление ключевых слов
      KeyWordGroup.where(:task_id => @item.id).each do |group|
        group.task_id = nil
        group.save
      end
      if !params[@param_name][:task_key_groups_ids].nil?
        params[@param_name][:task_key_groups_ids].each do |key, value|
          group = KeyWordGroup.find(key)
          group.task_id = @item.id
          group.save
        end
      end

      change_task_stage(@item)

      redirect_to @path
    else
      render "new"
    end

     #update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  def ajax_add_new_comment
    task = Task.find(params[:task_id])
    Comment.new({:text => params[:text], :system_user_id => current_system_user.id, :task_id => params[:task_id]}).save
    render :text => "Success"
  end

  def ajax_get_comments_by_task_id
    task = Task.find(params[:task_id])
    comments = task.comments
    if(!comments.nil?)
      key_words = comments.order(:created_at => :desc).map {|i| [i.text, i.created_at.strftime("%d.%m.%Y %H:%M:%S"), i.system_user_id, "#{SystemMainItemsD::SystemUser.find(i.system_user_id).first_name} #{SystemMainItemsD::SystemUser.find(i.system_user_id).last_name}", SystemMainItemsD::SystemUser.find(i.system_user_id).avatar.url(:thumb)] }
      render :json => key_words
    else
      render :json => []
    end
  end

  def ajax_get_project_data
    project = Project.find(params[:project_id])
    #project_content_managers = ProjectContentManager.where(:project_id => params[:project_id]).ma
    render :json => [project.key_manager_id, project.link_manager_id]
  end

  private

    def init_variables

      @object = {
          :name => "task",
          :entity => Task,
          :param_name => :task,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.tasks_path,
              :new_path => Rails.application.routes.url_helpers.new_task_path,
              :edit_path => Rails.application.routes.url_helpers.tasks_path
          },
          :breadcrumbs => {
              t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              t("modules.task.name") => nil
          },
          :fields => {
              :task_state_id => {
                  :type => :collection,
                  :label => t('form.labels.task_state_id'),
                  :show_in_table => false,
                  :show_in_card => false,
                  :where_entity => TaskState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  },
                  :available_key_manager => true
              },
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :available_key_manager => true
              },
              :date => {
                  :type => :datetime,
                  :label => t('form.labels.task_date'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :available_key_manager => true
              },
              :description => {
                  :type => :text,
                  :label => t('form.labels.description'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true,
                  :available_key_manager => true
              },
              :project_id => {
                  :type => :collection,
                  :label => t('form.labels.project_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => Project,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  },
                  :available_key_manager => false
              },
              :task_stage_id => {
                  :type => :collection,
                  :label => t('form.labels.task_stage_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => TaskStage,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  },
                  :available_key_manager => false
              },
              :label => {
                  :type => :group_label,
                  :label => "Настройка исполнителей",
                  :available_key_manager => false
              },
              :key_manager_id => {
                  :type => :collection,
                  :label => t('form.labels.key_manager_id'),
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemUser.where(:system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => "key_manager")),
                  :where_visible_field => :login,
                  :where_statement => {
                      :system_state_id => SystemMainItemsD::SystemState.active_state.id
                  },
                  :settings => {
                      :include_blank => false
                  },
                  :available_key_manager => false,
                  :help_message => t('form.messages.task_key_manager')
              },
              :link_manager_id => {
                  :type => :collection,
                  :label => t('form.labels.link_manager_id'),
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemUser.where(:system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => "link_manager")),
                  :where_visible_field => :login,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  },
                  :available_key_manager => false,
                  :help_message => t('form.messages.task_link_manager')
              },
              :task_content_managers => {
                  :type => :dual_list,
                  :label => t('form.labels.content_managers'),
                  :show_in_table => false,
                  :show_in_card => true,
                  :recipient => {
                      :name => :login,
                      :label => t('form.labels.currrent_content_managers'),
                      :entity => TaskContentManager,
                      :link_field => :task_id
                  },
                  :donor => {
                      :name => :login,
                      :label => t('form.labels.all_content_managers'),
                      :entity => SystemMainItemsD::SystemUser.where(:system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => "content_manager")),
                      :link_field => :system_user_id
                  },
                  :available_key_manager => false
              },
              :key_groups_ids => {
                :type => :empty,
                :show_in_table => false,
                :show_in_card => false,
                :available_key_manager => false
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end

