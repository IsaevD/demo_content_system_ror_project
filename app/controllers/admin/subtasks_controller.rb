class Admin::SubtasksController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    #create_item(@entity, @path, true)

    entity = @entity
    path = @path
    is_log = true
    is_redirect = true

    @item = entity.new(prepared_params(@param_name, @params_array))
    if @item.save
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :dual_list
            if settings[:show_in_card]
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], @item.id)
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
      end
      if (is_redirect)

        # Обновление ключевых слов
        Article.where(:subtask_id => @item.id).each do |article|
          article.subtask_id = nil
          article.save
        end
        if !params[@param_name][:subtasks_article_ids].nil?
          params[@param_name][:subtasks_article_ids].each do |key, value|
            article = Article.find(key)
            article.subtask_id = @item.id
            article.save
          end
        end

        redirect_to path
      end
    else
      render "new"
    end

  end

  def update
    #update_item(@entity, @path, params[:id], true)

    is_log = true
    id = params[:id]
    @item = @entity.find(params[:id])
    @fields.each do |field, settings|
      entity_params = params[@param_name]
      case settings[:type]
        when :image
          if entity_params[field] == "nil"
            params[@param_name][field] = nil
          end
        when :dual_list
          if settings[:show_in_card]
            current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
            current_values.destroy_all
            values = entity_params[field]
            if !values.nil?
              values.each do |key, value|
                settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
              end
            end
          end
        when :image_list
          if !entity_params[settings[:image_entity_field]].nil?
            entity_params[settings[:image_entity_field]].each do |key, value|
              if value == "nil"
                @fields[field][:image_entity].find(key).destroy
              end
            end
          end
          if !entity_params[field].nil?
            entity_params[field].each do |value|
              image_list = @fields[field][:image_entity].new
              image_list.update_attribute(@fields[field][:entity_field], params[:id])
              image_list.update_attribute(@fields[field][:image_entity_field], value)
              image_list.save
            end
          end
      end
    end

    if @item.update_attributes(prepared_params(@param_name, @params_array))
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
      end

      # Обновление ключевых слов
      if !params[@param_name][:subtask_purchase_link_ids].nil?
        params[@param_name][:subtask_purchase_link_ids].each do |key, value|
          link = PurchaseLink.find(key)
          link.subtask_id = @item.id
          link.save
        end
      end

      #Article.where(:subtask_id => @item.id).each do |article|
      #  article.subtask_id = nil
      #  article.save
      #end
      #if !params[@param_name][:subtask_articles_ids].nil?
      #  params[@param_name][:subtask_articles_ids].each do |key, value|
      #    article = Article.find(key)
      #    article.subtask_id = @item.id
      #    article.save
      #  end
      #end

      change_subtask_stage(@item)

      redirect_to @path
    else
      render "new"
    end

  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  def ajax_add_new_comment_to_subtask
    subtask_id = params[:subtask_id]
    Comment.new({:text => params[:text], :system_user_id => current_system_user.id, :subtask_id => subtask_id}).save
    render :text => "Success"
  end

  def ajax_get_comments_by_subtask_id
    subtask = Subtask.find(params[:subtask_id])
    comments = subtask.comments
    if(!comments.nil?)
      comments = comments.order(:created_at => :desc).map {|i| [i.text, i.created_at.strftime("%d.%m.%Y %H:%M:%S"), i.system_user_id, "#{SystemMainItemsD::SystemUser.find(i.system_user_id).first_name} #{SystemMainItemsD::SystemUser.find(i.system_user_id).last_name}", SystemMainItemsD::SystemUser.find(i.system_user_id).avatar.url(:thumb)] }
      render :json => comments
    else
      render :json => []
    end
  end

  private

    def init_variables

      @object = {
          :name => "subtask",
          :entity => Subtask,
          :param_name => :subtask,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.subtasks_path,
              :new_path => Rails.application.routes.url_helpers.new_subtask_path,
              :edit_path => Rails.application.routes.url_helpers.subtasks_path
          },
          :breadcrumbs => {
              t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              t("modules.subtask.name") => nil
          },
          :fields => {
              :task_state_id => {
                  :type => :collection,
                  :label => t('form.labels.task_state_id'),
                  :show_in_table => false,
                  :show_in_card => false,
                  :where_entity => TaskState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  },
                  :available_content_manager => true,
                  :available_link_manager => true
              },
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :available_content_manager => true,
                  :available_link_manager => true
              },
              :date => {
                  :type => :datetime,
                  :label => t('form.labels.task_date'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true,
                  :available_content_manager => true,
                  :available_link_manager => true
              },
              :description => {
                  :type => :text,
                  :label => t('form.labels.description'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true,
                  :available_content_manager => true,
                  :available_link_manager => true
              },
              :url => {
                  :type => :string,
                  :label => "Ссылка",
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true,
                  :available_content_manager => true,
                  :available_link_manager => true
              },
              :purchase_link_group_id => {
                  :type => :collection,
                  :label => "Покупные ссылки",
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => PurchaseLink,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => true
                  },
                  :available_content_manager => false
              },
              :label => {
                  :type => :group_label,
                  :label => "Контент"
              },
              :title => {
                  :type => :string,
                  :label => t('form.labels.title'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :available_content_manager => true,
                  :content_manager_setting => true
              },
              :content => {
                  :type => :html,
                  :label => t('form.labels.content'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :available_content_manager => true,
                  :content_manager_setting => true
              },
              :meta_title => {
                  :type => :string,
                  :label => t('form.labels.meta_title'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :available_content_manager => true,
                  :content_manager_setting => true
              },
              :meta_description => {
                  :type => :text,
                  :label => t('form.labels.meta_description'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :available_content_manager => true,
                  :content_manager_setting => true
              },
              :meta_keywords => {
                  :type => :text,
                  :label => t('form.labels.meta_keywords'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :available_content_manager => true,
                  :content_manager_setting => true
              },
              :tech_label => {
                  :type => :group_label,
                  :label => "Техническая информация",
                  :available_content_manager => false
              },
              :content_manager_id => {
                  :type => :collection,
                  :label => t('form.labels.content_manager_id'),
                  :show_in_table => false,
                  :show_in_card => false,
                  :where_entity => SystemMainItemsD::SystemUser,
                  :where_visible_field => :login,
                  :where_statement => {
                      :system_state_id => SystemMainItemsD::SystemState.active_state.id,
                      :system_user_role_id => SystemMainItemsD::SystemUserRole.content_manager_role.id
                  },
                  :settings => {
                      :include_blank => true
                  },
                  :available_content_manager => false
              },
              :link_manager_id => {
                  :type => :collection,
                  :label => "Link-менеджер",
                  :show_in_table => false,
                  :show_in_card => false,
                  :where_entity => SystemMainItemsD::SystemUser,
                  :where_visible_field => :login,
                  :where_statement => {
                      :system_state_id => SystemMainItemsD::SystemState.active_state.id,
                      :system_user_role_id => SystemMainItemsD::SystemUserRole.link_manager_role.id
                  },
                  :settings => {
                      :include_blank => true
                  }
              },
              :subtask_stage_id => {
                  :type => :collection,
                  :label => t('form.labels.subtask_stage'),
                  :show_in_table => false,
                  :show_in_card => true,
                  :where_entity => SubtaskStage,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end