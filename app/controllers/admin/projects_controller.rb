class Admin::ProjectsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

    def init_variables

      @object = {
          :name => "project",
          :entity => Project,
          :param_name => :project,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.projects_path,
              :new_path => Rails.application.routes.url_helpers.new_project_path,
              :edit_path => Rails.application.routes.url_helpers.projects_path
          },
          :breadcrumbs => {
              t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              t("modules.project.name") => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :url => {
                  :type => :string,
                  :label => t('form.labels.url'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :project_category_id => {
                  :type => :collection,
                  :label => t('form.labels.project_category_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => ProjectCategory,
                  :where_visible_field => :name,
                  :where_statement => {
                      :system_state_id => SystemMainItemsD::SystemState.active_state.id
                  },
                  :settings => {
                      :include_blank => false
                  }
              },
              :description => {
                  :type => :text,
                  :label => t('form.labels.description'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :key_manager_id =>  {
                  :type => :collection,
                  :label => t('form.labels.key_manager_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemUser,
                  :where_visible_field => :login,
                  :where_statement => {
                      :system_user_role_id => SystemMainItemsD::SystemUserRole.key_manager_role.id
                  },
                  :settings => {
                      :include_blank => false
                  },
                  :help_message => t('form.messages.project_key_manager')
              },
              :link_manager_id =>  {
                  :type => :collection,
                  :label => t('form.labels.link_manager_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemUser,
                  :where_visible_field => :login,
                  :where_statement => {
                    :system_user_role_id => SystemMainItemsD::SystemUserRole.link_manager_role.id
                  },
                  :settings => {
                      :include_blank => false
                  },
                  :help_message => t('form.messages.project_link_manager')
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  },
                  :help_message => t('form.messages.project_state')
              },
              :project_content_managers => {
                  :type => :dual_list,
                  :label => t('form.labels.content_managers'),
                  :show_in_table => false,
                  :show_in_card => true,
                  :recipient => {
                      :name => :login,
                      :label => t('form.labels.currrent_content_managers'),
                      :entity => ProjectContentManager,
                      :link_field => :project_id
                  },
                  :donor => {
                      :name => :login,
                      :label => t('form.labels.all_content_managers'),
                      :entity => SystemMainItemsD::SystemUser.where(:system_user_role_id => SystemMainItemsD::SystemUserRole.find_by(:alias => "content_manager")),
                      :link_field => :system_user_id
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end

