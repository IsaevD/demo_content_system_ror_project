class Admin::PurchaseLinksController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    #create_item(@entity, @path, true)

    entity = @entity
    path = @path
    is_log = true
    is_redirect = true

    @item = entity.new(prepared_params(@param_name, @params_array))

    @item.link_manager_id = current_system_user.id

    if @item.save
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :dual_list
            if settings[:show_in_card]
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], @item.id)
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
      end
      if (is_redirect)
        redirect_to path
      end
    else
      render "new"
    end

  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  def ajax_get_purchase_link
    pLink = PurchaseLink.find(params[:purchaseLink_id])
    render :json => [pLink.id, pLink.name]
  end

  private

    def init_variables

      @object = {
          :name => "purchase_link",
          :entity => PurchaseLink,
          :param_name => :purchase_link,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.purchase_links_path,
              :new_path => Rails.application.routes.url_helpers.new_purchase_link_path,
              :edit_path => Rails.application.routes.url_helpers.purchase_links_path
          },
          :breadcrumbs => {
              t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Покупные ссылки" => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :link => {
                  :type => :string,
                  :label => t('form.labels.link'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :price => {
                  :type => :string,
                  :label => t('form.labels.price'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :comment => {
                  :type => :text,
                  :label => t('form.labels.comment'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :article_id =>  {
                  :type => :collection,
                  :label => "Статья",
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => Article,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              },
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end

