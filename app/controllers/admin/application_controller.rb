class Admin::ApplicationController < ActionController::Base
  include DataProcessingD::ApplicationHelper
  include ObjectGeneratorD::ApplicationHelper
  include AuthorizeD::ApplicationHelper
  include SystemUsersAuthorizeD::ApplicationHelper
  include ActionView::Helpers::UrlHelper
  include AccessRightsD::ApplicationHelper
  include SystemUserActionsLogD::ApplicationHelper
  include TasksHelper
  layout "admin"
  helper ObjectGeneratorD::ApplicationHelper
  helper AuthorizeD::ApplicationHelper
  helper SystemUsersAuthorizeD::ApplicationHelper
  helper AccessRightsD::ApplicationHelper
  helper SystemUserActionsLogD::ApplicationHelper

  before_filter :init_app_variables, :signed_in_system?

  def signed_in_system?
    signed_in_user(system_users_authorize_d.sign_in_path, current_system_user)
  end


  private

    def init_app_variables
      items = params[:controller].split("/")
      @module_name = items[0]
      @module_item_alias = items[1]
      @module_item = ModulesD::ModuleItem.find_by(:module_name => @module_name, :alias => @module_item_alias)
      if signed_in(current_system_user)
        current_system_user
        menus = {
            "link_manager" => {
                :main_page => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.menu.main_page"),
                    :value => Rails.application.routes.url_helpers.main_page_path
                },
                :subtasks => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.menu.tasks"),
                    :value => Rails.application.routes.url_helpers.subtasks_path
                },
                :articles => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => "Статьи",
                    :value => Rails.application.routes.url_helpers.articles_path
                },
                :purchase_links => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => "Покупные ссылки",
                    :value => Rails.application.routes.url_helpers.purchase_links_path
                }
            },
            "key_manager" => {
              :main_page => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.main_page"),
                  :value => Rails.application.routes.url_helpers.main_page_path
              },
              :tasks => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.tasks"),
                  :value => Rails.application.routes.url_helpers.tasks_path
              },
              :key_word_groups => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.key_word_groups"),
                  :value => Rails.application.routes.url_helpers.key_word_groups_path
              },
              :key_words => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.key_words"),
                  :value => Rails.application.routes.url_helpers.key_words_path
              }
          },
            "content_manager" => {
                :main_page => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.menu.main_page"),
                    :value => Rails.application.routes.url_helpers.main_page_path
                },
                :auction => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.menu.auction"),
                    :value => Rails.application.routes.url_helpers.auction_path
                },
                :subtasks => {
                    :type => :url,
                    :icon => "icon-test",
                    :label => t("labels.menu.tasks"),
                    :value => Rails.application.routes.url_helpers.subtasks_path
                }
            },
          "administrator" => {
              :main_page => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.main_page"),
                  :value => Rails.application.routes.url_helpers.main_page_path
              },
              :tasks => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => t("labels.menu.manage_tasks"),
                  :value => {
                      :auction => {
                          :type => :url,
                          :label => "Аукцион",
                          :value => Rails.application.routes.url_helpers.auction_path
                      },
                      :projects => {
                          :type => :url,
                          :label => t("labels.menu.tasks"),
                          :value => Rails.application.routes.url_helpers.tasks_path
                      }
                  }
              },
              :projects => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => t("labels.menu.manage_projects"),
                  :value => {
                      :projects => {
                          :type => :url,
                          :label => t("labels.menu.projects"),
                          :value => Rails.application.routes.url_helpers.projects_path
                      },
                      :project_categories => {
                          :type => :url,
                          :label => t("labels.menu.project_categories"),
                          :value => Rails.application.routes.url_helpers.project_categories_path
                      }
                  }
              },
              :key_words => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => t("labels.menu.manage_key_words"),
                  :value => {
                      :key_word_groups => {
                          :type => :url,
                          :label => t("labels.menu.key_word_groups"),
                          :value => Rails.application.routes.url_helpers.key_word_groups_path
                      },
                      :key_words => {
                          :type => :url,
                          :label => t("labels.menu.key_words"),
                          :value => Rails.application.routes.url_helpers.key_words_path
                      },
                      :key_word_rubrics => {
                          :type => :url,
                          :label => t("labels.menu.key_word_rubrics"),
                          :value => Rails.application.routes.url_helpers.key_word_rubrics_path
                      },
                      :key_word_states => {
                          :type => :url,
                          :label => t("labels.menu.key_word_state"),
                          :value => Rails.application.routes.url_helpers.key_word_states_path
                      }
                  }
              },
              :purchase_links => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => "Управление покупными ссылками",
                  :value => {
                      :articles => {
                          :type => :url,
                          :label => "Статьи",
                          :value => Rails.application.routes.url_helpers.articles_path
                      },
                      :purchase_links => {
                          :type => :url,
                          :label => "Покупные ссылки",
                          :value => Rails.application.routes.url_helpers.purchase_links_path
                      }
                  }
              },
              :news => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.news"),
                  :value => Rails.application.routes.url_helpers.news_items_path
              },
              :instructions => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.instructions"),
                  :value => Rails.application.routes.url_helpers.instructions_path
              }
          },
          "root" => {
              :main_page => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.main_page"),
                  :value => Rails.application.routes.url_helpers.main_page_path
              },
              :tasks => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => t("labels.menu.manage_tasks"),
                  :value => {
                      :auction => {
                          :type => :url,
                          :label => "Аукцион",
                          :value => Rails.application.routes.url_helpers.auction_path
                      },
                      :projects => {
                          :type => :url,
                          :label => t("labels.menu.tasks"),
                          :value => Rails.application.routes.url_helpers.tasks_path
                      }
                  }
              },
              :projects => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => t("labels.menu.manage_projects"),
                  :value => {
                      :projects => {
                          :type => :url,
                          :label => t("labels.menu.projects"),
                          :value => Rails.application.routes.url_helpers.projects_path
                      },
                      :project_categories => {
                          :type => :url,
                          :label => t("labels.menu.project_categories"),
                          :value => Rails.application.routes.url_helpers.project_categories_path
                      }
                  }
              },
              :key_words => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => t("labels.menu.manage_key_words"),
                  :value => {
                      :key_word_groups => {
                          :type => :url,
                          :label => t("labels.menu.key_word_groups"),
                          :value => Rails.application.routes.url_helpers.key_word_groups_path
                      },
                      :key_words => {
                          :type => :url,
                          :label => t("labels.menu.key_words"),
                          :value => Rails.application.routes.url_helpers.key_words_path
                      },
                      :key_word_rubrics => {
                          :type => :url,
                          :label => t("labels.menu.key_word_rubrics"),
                          :value => Rails.application.routes.url_helpers.key_word_rubrics_path
                      },
                      :key_word_states => {
                          :type => :url,
                          :label => t("labels.menu.key_word_state"),
                          :value => Rails.application.routes.url_helpers.key_word_states_path
                      }
                  }
              },
              :purchase_links => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => "Управление покупными ссылками",
                  :value => {
                      :articles => {
                          :type => :url,
                          :label => "Статьи",
                          :value => Rails.application.routes.url_helpers.articles_path
                      },
                      :purchase_links => {
                          :type => :url,
                          :label => "Покупные ссылки",
                          :value => Rails.application.routes.url_helpers.purchase_links_path
                      }
                  }
              },
              :access_rules => {
                  :type => :two_step_menu,
                  :icon => "icon-test",
                  :label => t("labels.menu.users_manage"),
                  :value => {
                      :system_users => {
                          :type => :url,
                          :label => t("labels.menu.system_users"),
                          :value => system_main_items_d.system_users_path
                      },
                      :system_user_roles => {
                          :type => :url,
                          :label => t("labels.menu.system_user_roles"),
                          :value => system_main_items_d.system_user_roles_path
                      }
                  }
              },
              :news => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.news"),
                  :value => Rails.application.routes.url_helpers.news_items_path
              },
              :instructions => {
                  :type => :url,
                  :icon => "icon-test",
                  :label => t("labels.menu.instructions"),
                  :value => Rails.application.routes.url_helpers.instructions_path
              }
          }
        }
        @menu_items = menus[current_system_user.system_user_role.alias]
      end

    end


end
