class Admin::NewsItemsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

    def init_variables

      @object = {
          :name => "news_items",
          :entity => NewsItem,
          :param_name => :news_item,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.news_items_path,
              :new_path => Rails.application.routes.url_helpers.new_news_item_path,
              :edit_path => Rails.application.routes.url_helpers.news_items_path
          },
          :breadcrumbs => {
              t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              t("modules.news_items.name") => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :description => {
                  :type => :html,
                  :label => t('form.labels.content'),
                  :show_in_card => true,
                  :show_in_table => false,
                  :required => true
              },
              :date =>  {
                  :type => :datetime,
                  :label => t('form.labels.date'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :system_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.system_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => SystemMainItemsD::SystemState,
                  :where_visible_field => :name,
                  :where_statement => nil,
                  :settings => {
                      :include_blank => false
                  }
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end

