class Admin::KeyWordsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    path = @path
    is_log = true
    is_redirect = true
    @item = @entity.new(prepared_params(@param_name, @params_array))
    @item.key_manager_id = current_system_user.id
    if @item.save
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :dual_list
            if settings[:show_in_card]
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], @item.id)
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
      end
      if (is_redirect)
        @item.key_manager_id = current_system_user.id
        redirect_to path
      end
    else
      render "new"
    end

    #create_item(@entity, @path, true)
  end

  def update
    update_item(@entity, @path, params[:id], true)
  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  private

    def init_variables

      if current_system_user.system_user_role.alias == "administrator"
        key_word_group_where = nil
      else
        key_word_group_where = {
            :key_manager_id => current_system_user.id
        }
      end

      @object = {
          :name => "key_words",
          :entity => KeyWord,
          :param_name => :key_word,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.key_words_path,
              :new_path => Rails.application.routes.url_helpers.new_key_word_path,
              :edit_path => Rails.application.routes.url_helpers.key_words_path
          },
          :breadcrumbs => {
              t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              t("modules.key_words.name") => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :key_word_group_id =>  {
                  :type => :collection,
                  :label => t('form.labels.key_word_group_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => KeyWordGroup,
                  :where_visible_field => :name,
                  :where_statement => key_word_group_where,
                  :settings => {
                      :include_blank => false
                  }
              },
              :key_word_state_id =>  {
                  :type => :collection,
                  :label => t('form.labels.key_word_state_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => KeyWordState,
                  :where_visible_field => :name,
                  :where_statement => {
                      :system_state_id => SystemMainItemsD::SystemState.active_state.id
                  },
                  :settings => {
                      :include_blank => false
                  }
              },
              :entry_count => {
                  :type => :string,
                  :label => t('form.labels.entry_count'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => false
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end

