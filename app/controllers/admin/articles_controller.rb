class Admin::ArticlesController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create
    #create_item(@entity, @path, true)

    entity = @entity
    path = @path
    is_log = true
    is_redirect = true

    @item = entity.new(prepared_params(@param_name, @params_array))
    @item.link_manager_id = current_system_user.id
    if @item.save
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :dual_list
            if settings[:show_in_card]
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], @item.id)
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
      end
      if (is_redirect)

        # Обновление покупных ссылок
        PurchaseLink.where(:article_id => @item.id).each do |purchase_link|
          purchase_link.article_id = nil
          purchase_link.save
        end
        if !params[:purchase_links].nil?
          params[:purchase_links].each do |key, value|
            if key.to_i > 0
              purchase_link = PurchaseLink.find(value[:id])
              purchase_link.article_id = @item.id
              purchase_link.link_manager_id = @item.key_manager_id
            else
              purchase_link = PurchaseLink.new
              purchase_link.name = value[:name]
              purchase_link.link = value[:link]
              purchase_link.price = value[:price]
              purchase_link.comment = value[:comment]
              purchase_link.article_id = @item.id
              purchase_link.link_manager_id = current_system_user.id
            end
            purchase_link.save
          end
        end

        redirect_to path
      end
    else
      render "new"
    end

  end

  def update
    #update_item(@entity, @path, params[:id], true)

    entity = @entity
    path = @path
    id = params[:id]
    is_log = true

    @item = entity.find(id)

    @fields.each do |field, settings|
      entity_params = params[@param_name]
      case settings[:type]
        when :image
          if entity_params[field] == "nil"
            params[@param_name][field] = nil
          end
        when :dual_list
          if settings[:show_in_card]
            current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
            current_values.destroy_all
            values = entity_params[field]
            if !values.nil?
              values.each do |key, value|
                settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
              end
            end
          end
        when :image_list
          if !entity_params[settings[:image_entity_field]].nil?
            entity_params[settings[:image_entity_field]].each do |key, value|
              if value == "nil"
                @fields[field][:image_entity].find(key).destroy
              end
            end
          end
          if !entity_params[field].nil?
            entity_params[field].each do |value|
              image_list = @fields[field][:image_entity].new
              image_list.update_attribute(@fields[field][:entity_field], params[:id])
              image_list.update_attribute(@fields[field][:image_entity_field], value)
              image_list.save
            end
          end
      end
    end

    if @item.update_attributes(prepared_params(@param_name, @params_array))
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
      end

      # Обновление ключевых слов
      PurchaseLink.where(:article_id => @item.id).each do |purchase_link|
        purchase_link.article_id = nil
        purchase_link.save
      end
      if !params[:purchase_links].nil?
        params[:purchase_links].each do |key, value|
          if key.to_i > 0
            purchase_link = PurchaseLink.find(value[:id])
            purchase_link.article_id = @item.id
            purchase_link.link_manager_id = @item.link_manager_id
          else
            purchase_link = PurchaseLink.new
            purchase_link.name = value[:name]
            purchase_link.link = value[:link]
            purchase_link.price = value[:price]
            purchase_link.comment = value[:comment]
            purchase_link.article_id = @item.id
            purchase_link.link_manager_id = current_system_user.id
          end
          purchase_link.save
        end
      end

      redirect_to path
    else
      render "new"
    end

  end

  def destroy
    delete_item(@entity, @path, params[:id], true)
  end

  def ajax_get_article
    article = Article.find(params[:article_id])
    links = article.purchase_links.map {|i| [i.id, i.name] }
    render :json => [article.id, article.name, links]
  end

  private

    def init_variables

      @object = {
          :name => "articles",
          :entity => Article,
          :param_name => :article,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.articles_path,
              :new_path => Rails.application.routes.url_helpers.new_article_path,
              :edit_path => Rails.application.routes.url_helpers.articles_path
          },
          :breadcrumbs => {
              t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              "Статьи" => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end

