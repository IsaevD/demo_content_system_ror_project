class Admin::IndexController < Admin::ApplicationController

  def index
    if params[:role_alias].nil?
      @role_alias = "none"
    else
      @role_alias = params[:role_alias]
    end
    @instructions = Instruction.where(:system_user_role_id => current_system_user.system_user_role.id)
  end

  def news
  end

  def instruction_view
  end

  def auction
    #@subtasks = Subtask.where(:content_manager_id => nil).order(:updated_at => :desc)
    @subtasks = Subtask.get_auction_subtasks_by_content_manager(current_system_user)
  end

  def ajax_get_subtask_from_auction
    subtask_id = params[:subtask_id]
    subtask = Subtask.find(subtask_id)
    if subtask.subtask_stage.alias == SubtaskStage.AUCTION_CONTENT_MANAGER_WORK_STAGE_ALIAS
      subtask.content_manager_id = current_system_user.id
      subtask.subtask_stage = SubtaskStage.content_manager_work_stage
      subtask.save
    end
    render :text => "Success"
  end


  def ajax_auction_subtasks
    render :json => Subtask.where(:subtask_stage_id => SubtaskStage.auction_content_manager_work_stage.id).order(:updated_at => :desc).map {|i| [i.id, i.task.project.name, i.name,  "#{i.key_word_group.volume_of} - #{i.key_word_group.volume_to}", "#{(i.key_word_group.volume_of.to_f / 1000 * current_system_user.rate.to_f).round()} - #{(i.key_word_group.volume_to.to_f / 1000 * current_system_user.rate.to_f).round()} руб." ]}
  end

end