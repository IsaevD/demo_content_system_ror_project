class Admin::KeyWordGroupsController < Admin::ApplicationController
  before_filter :init_variables

  def index
    all_item(@entity, params[:filter], true, true, true)
  end

  def show
    get_item_by_id(@entity, params[:id], true)
  end

  def new
    new_item(@entity)
  end

  def edit
    get_item_by_id(@entity, params[:id], true)
  end

  def create

    path = @path
    is_log = true
    is_redirect = true

    @item = @entity.new(prepared_params(@param_name, @params_array))
    @item.key_manager_id = current_system_user.id
    if @item.save
      @fields.each do |field, settings|
        entity_params = params[@param_name]
        case settings[:type]
          when :dual_list
            if settings[:show_in_card]
              values = entity_params[field]
              if !values.nil?
                values.each do |key, value|
                  settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
                end
              end
            end
          when :image_list
            if !entity_params[field].nil?
              entity_params[field].each do |value|
                image_list = @fields[field][:image_entity].new
                image_list.update_attribute(@fields[field][:entity_field], @item.id)
                image_list.update_attribute(@fields[field][:image_entity_field], value)
                image_list.save
              end
            end
        end
      end
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.CREATE_ENTITY_ALIAS, @item.id)
      end
      if (is_redirect)

        # Обновление ключевых слов
        KeyWord.where(:key_word_group_id => @item.id).each do |key_word|
          key_word.key_word_group_id = nil
          key_word.save
        end
        if !params[:key_words].nil?
          params[:key_words].each do |key, value|
            if key.to_i > 0
              key_word = KeyWord.find(value[:id])
              key_word.key_word_group_id = @item.id
            else
              key_word = KeyWord.new
              key_word.name = value[:name]
              key_word.key_word_state_id = value[:state_id]
              key_word.entry_count = value[:entry_count]
              key_word.key_word_group_id = @item.id
              key_word.key_manager_id = current_system_user.id
              key_word.system_state_id = SystemMainItemsD::SystemState.active_state.id
            end
            key_word.key_manager_id = @item.key_manager_id
            key_word.save
          end
        end

        redirect_to path
      end
    else
      render "new"
    end

    #create_item(@entity, @path, true)
  end

  def update
    #update_item(@entity, @path, params[:id], true)

    entity = @entity
    path = @path
    id = params[:id]
    is_log = true

    @item = entity.find(id)

    @fields.each do |field, settings|
      entity_params = params[@param_name]
      case settings[:type]
        when :image
          if entity_params[field] == "nil"
            params[@param_name][field] = nil
          end
        when :dual_list
          if settings[:show_in_card]
            current_values = settings[:recipient][:entity].where(settings[:recipient][:link_field] => @item.id)
            current_values.destroy_all
            values = entity_params[field]
            if !values.nil?
              values.each do |key, value|
                settings[:recipient][:entity].new({settings[:recipient][:link_field] => @item.id, settings[:donor][:link_field] => key }).save
              end
            end
          end
        when :image_list
          if !entity_params[settings[:image_entity_field]].nil?
            entity_params[settings[:image_entity_field]].each do |key, value|
              if value == "nil"
                @fields[field][:image_entity].find(key).destroy
              end
            end
          end
          if !entity_params[field].nil?
            entity_params[field].each do |value|
              image_list = @fields[field][:image_entity].new
              image_list.update_attribute(@fields[field][:entity_field], params[:id])
              image_list.update_attribute(@fields[field][:image_entity_field], value)
              image_list.save
            end
          end
      end
    end

    if @item.update_attributes(prepared_params(@param_name, @params_array))
      if (is_log)
        create_action_log_record(SystemUserActionsLogD::LogRecordState.UPDATE_ENTITY_ALIAS, id)
      end

      # Обновление ключевых слов
      KeyWord.where(:key_word_group_id => @item.id).each do |key_word|
        key_word.key_word_group_id = nil
        key_word.save
      end
      if !params[:key_words].nil?
        params[:key_words].each do |key, value|
          if key.to_i > 0
            key_word = KeyWord.find(value[:id])
            key_word.key_word_group_id = @item.id
            key_word.name = value[:name] unless value[:name].nil?
            key_word.key_word_state_id = value[:state_id] unless value[:state_id].nil?
            key_word.entry_count = value[:entry_count] unless value[:entry_count].nil?
            key_word.save
          else
            key_word = KeyWord.new
            key_word.name = value[:name]
            key_word.key_word_state_id = value[:state_id]
            key_word.entry_count = value[:entry_count]
            key_word.key_word_group_id = @item.id
            key_word.key_manager_id = current_system_user.id
            key_word.system_state_id = SystemMainItemsD::SystemState.active_state.id
            key_word.save
          end
        end
      end

      redirect_to path
    else
      render "new"
    end

  end

  def destroy
    #delete_item(@entity, @path, params[:id], true)

    entity = @entity
    path = @path
    id = params[:id]
    is_log = true

    @item = entity.find(id)
    @item.key_words.each do |key_word|
      key_word.key_word_group = nil
      key_word.save
    end

    @item.destroy
    if (is_log)
      create_action_log_record(SystemUserActionsLogD::LogRecordState.DELETE_ENTITY_ALIAS, id)
    end
    redirect_to path

  end

  def ajax_get_key_word_group
    key_word_group = KeyWordGroup.find(params[:group_id])
    key_words = key_word_group.key_words.map {|i| [i.id, i.name] }
    render :json => [key_word_group.id, key_word_group.name, key_words]
  end

  private

    def init_variables

      @object = {
          :name => "key_word_groups",
          :entity => KeyWordGroup,
          :param_name => :key_word_group,
          :paths => {
              :all_path => Rails.application.routes.url_helpers.key_word_groups_path,
              :new_path => Rails.application.routes.url_helpers.new_key_word_group_path,
              :edit_path => Rails.application.routes.url_helpers.key_word_groups_path
          },
          :breadcrumbs => {
              t("modules.main_page.name") => Rails.application.routes.url_helpers.main_page_path,
              t("modules.key_word_groups.name") => nil
          },
          :fields => {
              :name => {
                  :type => :string,
                  :label => t('form.labels.name'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :key_word_rubric_id =>  {
                  :type => :collection,
                  :label => t('form.labels.key_word_rubric_id'),
                  :show_in_table => true,
                  :show_in_card => true,
                  :where_entity => KeyWordRubric,
                  :where_visible_field => :name,
                  :where_statement => {
                      :system_state_id => SystemMainItemsD::SystemState.active_state.id
                  },
                  :settings => {
                      :include_blank => false
                  }
              },
              :description => {
                  :type => :text,
                  :label => t('form.labels.description'),
                  :show_in_card => true,
                  :show_in_table => false
              },
              :personal_data => {
                  :type => :group_label,
                  :label => t("custom.labels.techinal_info")
              },
              :volume_of => {
                  :type => :string,
                  :label => t('form.labels.volume_of'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :volume_to => {
                  :type => :string,
                  :label => t('form.labels.volume_to'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              },
              :exact_entry => {
                  :type => :string,
                  :label => t('form.labels.exact_entry'),
                  :show_in_card => true,
                  :show_in_table => true,
                  :required => true
              }
          }
      }
      @fields = @object[:fields]
      @visible_fields = []
      @fields.each do |key, value|
        if value[:show_in_table]
          @visible_fields << key
        end
      end
      @entity = @object[:entity]
      @param_name = @object[:param_name]
      @path = @object[:paths][:all_path]
      @params_array, @field_labels = @fields.map { |k,v| [k.to_s, v.to_s] }.transpose
    end
end

